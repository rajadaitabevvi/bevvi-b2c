export const environment = {
    production: true,
    googleAnalytics: {
        domain: 'auto', // 'none' for localhost           'auto' when going live
        trackingId: 'UA-114567982-1'
    }
};
