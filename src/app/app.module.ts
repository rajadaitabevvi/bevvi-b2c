import { GoogleMapsAPIWrapper } from '@agm/core';
import { DirectionsMapDirective } from './shared/directive/sebm-google-map-directions';
import { AuthGuard } from './auth.guard';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { SocialLoginModule, AuthServiceConfig } from "angular4-social-login";
import { FacebookLoginProvider } from "angular4-social-login";
import { IonRangeSliderModule } from "ng2-ion-range-slider";

import { AppComponent } from './app.component';
import { FrontendComponent } from './web/frontend.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BackendComponent } from './admin/backend.component';
import { AccountinfoComponent } from './web/accountinfo/accountinfo.component';
import { CartCheckoutComponent } from './web/cart-checkout/cart-checkout.component';
import { CartLoginComponent } from './web/cart-login/cart-login.component';
import { CartComponent } from './web/cart/cart.component';
import { HelpComponent } from './web/help/help.component';
import { LoginComponent } from './web/login/login.component';
import { MyordersComponent } from './web/myorders/myorders.component';
import { PaymentoptionsComponent } from './web/paymentoptions/paymentoptions.component';
import { PrimarypickupComponent } from './web/primarypickup/primarypickup.component';
import { ProductdetailComponent } from './web/productdetail/productdetail.component';
import { ProductlistingComponent } from './web/productlisting/productlisting.component';
import { ProfileComponent } from './web/profile/profile.component';
import { ViewreceiptComponent } from './web/viewreceipt/viewreceipt.component';
import { ProviderServiceService } from "../app/shared/services/provider-service.service";
import { DaysLeftPipe } from './shared/pipes/days-left.pipe';
import { UserService } from './shared/services/user.service';
import { CalculateCartPricePipe } from './shared/pipes/calculate-cart-price.pipe';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { CalculateSavedPricePipe } from './shared/pipes/calculate-saved-price.pipe';
import { CalculatePointsEarnedPipe } from './shared/pipes/calculate-points-earned.pipe';
import { CalculateSubtotalPipe } from './shared/pipes/calculate-subtotal.pipe';
import { IndexComponent } from './web/index/index.component';
import { ForgotpasswordComponent } from './web/forgotpassword/forgotpassword.component';
import { ResetPasswordComponent } from './web/reset-password/reset-password.component';
import { CalculateTotalPricePipe } from './shared/pipes/calculate-total-price.pipe';
import { CallbackPipe } from './shared/pipes/callback.pipe';
import { AddPaymentCardComponent } from './web/add-payment-card/add-payment-card.component';
import { CardImagePipe } from './shared/pipes/card-image.pipe';
import { FACEBOOK_APP_ID, GOOGLE_API_KEY } from './constants/constants';
import { NoAddressAvailableComponent } from './web/no-address-available/no-address-available.component';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { CartCheckoutPaymentComponent } from './web/cart-checkout-payment/cart-checkout-payment.component';
import { MomentModule } from 'angular2-moment';
import { MomentTimezoneModule } from 'angular-moment-timezone';
import { CartCheckoutSummaryComponent } from './web/cart-checkout-summary/cart-checkout-summary.component';
import { OrderConfirmationComponent } from './web/order-confirmation/order-confirmation.component';
import { CalculateTaxPipe } from './shared/pipes/calculate-tax.pipe';
import { TotalCartItemsPipe } from './shared/pipes/total-cart-items.pipe';
import { PointsBarPipe } from './shared/pipes/points-bar.pipe';
import { ReviewProductComponent } from './web/review-product/review-product.component';
import { NgXCreditCardsModule } from 'ngx-credit-cards';
import { DatetimepickerComponent } from './web/datetimepicker.component';
import { StarRatingModule } from 'angular-star-rating';
import { OwlModule } from 'ngx-owl-carousel';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Angulartics2Facebook } from 'angulartics2/facebook'
import { SpinnerModule } from '@chevtek/angular-spinners';
import { ClipboardDirective } from "./shared/directive/clipboard.directive";
import { ClipboardService } from "./shared/services/clipboard.service";

import * as Payment from 'payment';
import { ClickStopPropagationDirective } from './shared/directive/click-stop-propagation.directive';
import { PrivacyPolicyComponent } from './web/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './web/terms-of-service/terms-of-service.component';
import { NoStoresAvailableComponent } from './web/no-stores-available/no-stores-available.component';
import { ProductImageUrlPipe } from './shared/pipes/product-image-url.pipe';
Payment.fns.restrictNumeric = Payment.restrictNumeric;
Payment.fns.formatCardExpiry = Payment.formatCardExpiry;
Payment.fns.formatCardCVC = Payment.formatCardCVC;

let config = new AuthServiceConfig([
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(FACEBOOK_APP_ID)
    }
]);

export function provideConfig() {
    return config;
}

@NgModule({
    declarations: [
        AppComponent,
        FrontendComponent,
        PageNotFoundComponent,
        BackendComponent,
        AccountinfoComponent,
        CartCheckoutComponent,
        CartLoginComponent,
        CartComponent,
        HelpComponent,
        LoginComponent,
        MyordersComponent,
        PaymentoptionsComponent,
        PrimarypickupComponent,
        ProductdetailComponent,
        ProductlistingComponent,
        ProfileComponent,
        ViewreceiptComponent,
        DaysLeftPipe,
        CalculateCartPricePipe,
        CalculateSavedPricePipe,
        CalculatePointsEarnedPipe,
        CalculateSubtotalPipe,
        IndexComponent,
        ForgotpasswordComponent,
        ResetPasswordComponent,
        CalculateTotalPricePipe,
        CallbackPipe,
        AddPaymentCardComponent,
        CardImagePipe,
        NoAddressAvailableComponent,
        CartCheckoutPaymentComponent,
        CartCheckoutSummaryComponent,
        OrderConfirmationComponent,
        CalculateTaxPipe,
        TotalCartItemsPipe,
        PointsBarPipe,
        ReviewProductComponent,
        DatetimepickerComponent,
        ClickStopPropagationDirective,
        PrivacyPolicyComponent,
        TermsOfServiceComponent,
        ClipboardDirective,
        DirectionsMapDirective,
        NoStoresAvailableComponent,
        ProductImageUrlPipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        SocialLoginModule,
        HttpModule,
        FlashMessagesModule,
        IonRangeSliderModule,
        NgXCreditCardsModule,
        AgmCoreModule.forRoot({
            apiKey: GOOGLE_API_KEY,
            libraries: ["places"]
        }),
        MomentModule,
        MomentTimezoneModule,
        StarRatingModule.forRoot(),
        OwlModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics, Angulartics2Facebook]),
        SpinnerModule
    ],
    providers: [
        ProviderServiceService,
        UserService,
        AuthGuard,
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        },
        GoogleMapsAPIWrapper,
        ClipboardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
