import { NoStoresAvailableComponent } from './no-stores-available/no-stores-available.component';
import { CartLoginComponent } from './cart-login/cart-login.component';
import { CartCheckoutSummaryComponent } from './cart-checkout-summary/cart-checkout-summary.component';
import { CartCheckoutPaymentComponent } from './cart-checkout-payment/cart-checkout-payment.component';
import { NoAddressAvailableComponent } from './no-address-available/no-address-available.component';
import { AddPaymentCardComponent } from './add-payment-card/add-payment-card.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ViewreceiptComponent } from './viewreceipt/viewreceipt.component';
import { ProfileComponent } from './profile/profile.component';
import { ProductlistingComponent } from './productlisting/productlisting.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { PrimarypickupComponent } from './primarypickup/primarypickup.component';
import { PaymentoptionsComponent } from './paymentoptions/paymentoptions.component';
import { MyordersComponent } from './myorders/myorders.component';
import { HelpComponent } from './help/help.component';
import { CartCheckoutComponent } from './cart-checkout/cart-checkout.component';
import { AccountinfoComponent } from './accountinfo/accountinfo.component';
import { LoginComponent } from './login/login.component';
import { Routes, CanActivate } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { AuthGuard } from './../auth.guard';
import { IndexComponent } from './index/index.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ReviewProductComponent } from './review-product/review-product.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './terms-of-service/terms-of-service.component';

export const FRONTEND_ROUTES: Routes = [
    {
        path: '',
        component: IndexComponent
    },
    {
        path: 'forgotpassword',
        component: ForgotpasswordComponent
    },
    {
        path: 'nopickupaddress',
        component: NoAddressAvailableComponent
    },
    {
        path: 'nostoresavailable',
        component: NoStoresAvailableComponent
    },
    {
        path: 'reset-password/:accessToken/:accountId',
        component: ResetPasswordComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'accountinfo',
        canActivate: [AuthGuard],
        component: AccountinfoComponent
    },
    {
        path: 'cart',
        component: CartComponent
    },
    {
        path: 'cart-login',
        component: CartLoginComponent
    },
    {
        path: 'cart-checkout',
        component: CartCheckoutComponent
    },
    {
        path: 'cart-payment',
        canActivate: [AuthGuard],
        component: CartCheckoutPaymentComponent
    },
    {
        path: 'cart-checkout-summary',
        canActivate: [AuthGuard],
        component: CartCheckoutSummaryComponent
    },
    {
        path: 'order-confirmation/:orderId',
        canActivate: [AuthGuard],
        component: OrderConfirmationComponent
    },
    {
        path: 'help',
        component: HelpComponent
    },
    {
        path: 'myorders',
        canActivate: [AuthGuard],
        component: MyordersComponent
    },
    {
        path: 'paymentoptions',
        canActivate: [AuthGuard],
        component: PaymentoptionsComponent
    },
    {
        path: 'add-payment',
        canActivate: [AuthGuard],
        component: AddPaymentCardComponent
    },
    {
        path: 'primarypickup',
        canActivate: [AuthGuard],
        component: PrimarypickupComponent
    },
    {
        path: 'productdetail/:offerId',
        // canActivate: [AuthGuard],
        component: ProductdetailComponent
    },
    {
        path: 'productlisting',
        component: ProductlistingComponent
    },
    {
        path: 'productlisting/:establishmentId',
        component: ProductlistingComponent
    },
    {
        path: 'productlisting/:establishmentId/:productCategory',
        component: ProductlistingComponent
    },
    {
        path: 'profile',
        canActivate: [AuthGuard],
        component: ProfileComponent
    },
    {
        path: 'view-receipt/:orderId',
        canActivate: [AuthGuard],
        component: ViewreceiptComponent
    },
    {
        path: 'review-product/:orderId',
        canActivate: [AuthGuard],
        component: ReviewProductComponent
    },
    {
        path: 'privacyPolicy',
        component: PrivacyPolicyComponent
    },
    {
        path: 'termsOfUse',
        component: TermsOfServiceComponent
    }
];
