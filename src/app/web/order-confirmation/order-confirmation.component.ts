import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';
import { ElementRef, NgModule, ViewChild } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { Router } from '@angular/router';
import { Angulartics2 } from 'angulartics2';
import { BRANCH_LINK } from '../../constants/constants';
import * as $ from "jquery";


@Component({
    selector: 'app-order-confirmation',
    templateUrl: './order-confirmation.component.html',
    styleUrls: ['./order-confirmation.component.css']
})
export class OrderConfirmationComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public accountData: Array<any>;
    public establishmentData: Array<any>;
    public orderDetails: Array<any>;
    public orderData: Array<any>;
    public establishmentLatitude: string;
    public establishmentLongitude: string;
    public zoom: number = 16;
    public userTimezone: string = Intl.DateTimeFormat().resolvedOptions().timeZone;
    public branchLink: string = BRANCH_LINK;

    constructor(
        private route: ActivatedRoute,
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private router: Router,
        private angulartics2: Angulartics2
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.route.params
            .subscribe(params => {
                this.ProviderServiceService.getOrderDetails(params.orderId)
                    .subscribe(data => {
                        this.ProviderServiceService.emptyShoppingCart(1)
                            .subscribe(data => {
                                this.userService.setHeaderCartCheck('true');
                            });
                        this.angulartics2.eventTrack.next({
                            action: 'Purchase',
                            properties: {
                                category: 'Purchase',
                                label: 'action',
                                value: "User Completed the Purchase",
                                orderId: params.orderId
                            },
                        });
                        data = data[0];
                        this.orderData = data;
                        this.userService.setDiscountInfo(data.discountApplied.discountAmount);
                        this.accountData = data.account;
                        this.establishmentData = data.establishment;
                        this.orderDetails = data.orderdetails;
                        delete this.orderData['account'];
                        delete this.orderData['establishment'];
                        delete this.orderData['orderdetails'];
                        this.establishmentLatitude = this.establishmentData['geoLocation'].coordinates[1];
                        this.establishmentLongitude = this.establishmentData['geoLocation'].coordinates[0];
                        this.userService.hideAppSpinner();
                    });
            });
    }


    public logError(error: Error): void {

        console.group("Clipboard Error");
        console.error(error);
        console.groupEnd();

    }


    // I log Clipboard "copy" successes.
    public logSuccess(value: string): void {
        console.group("Clipboard Success");
        $("#copyText").text("Copied");
        console.groupEnd();
    }

}
