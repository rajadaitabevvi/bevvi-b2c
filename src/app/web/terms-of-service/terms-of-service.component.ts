import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
    selector: 'app-terms-of-service',
    templateUrl: './terms-of-service.component.html',
    styleUrls: ['./terms-of-service.component.css']
})
export class TermsOfServiceComponent implements OnInit {

    constructor(
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        // if (this.route.snapshot.queryParams["fromMobile"]) {
            $('header, footer').addClass('hidden');
            $('.profile-head').css("margin-top", "0px");
        // }
    }

}
