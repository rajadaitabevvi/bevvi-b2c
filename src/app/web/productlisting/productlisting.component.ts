import { Router, ActivatedRoute } from '@angular/router';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { API_URL, GOOGLE_API_KEY } from '../../constants/constants';
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import * as $ from "jquery";
import request from 'request';

@Component({
    selector: 'app-productlisting',
    templateUrl: './productlisting.component.html',
    styleUrls: ['./productlisting.component.css']
})
export class ProductlistingComponent implements OnInit {

    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public productListing: Array<any> = [];
    public storeListing: Array<any> = [];
    public allProductListing: Array<any> = [];
    public allStoreListing: Array<any> = [];
    public tempStoreListing: Array<any> = [];
    public storeIds: Array<any> = [];
    public establishmentData: any = {};
    public timeInc: number = 0.2;
    public productFilterArray: Array<any> = [];
    public storeFilterArray: Array<any> = [];
    public userCurrentLocation: any = {};
    public geolocationPosition: any = {};
    public currentUserDetails: any = (this.userService.isLoggedIn()) ? this.userService.getCurrentUserData() : {};
    public userPickupAddress: Array<any> = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
    public userPickupAddressToUse: Object = (this.userService.getUserPickupLocationToUse()) ? this.userService.getUserPickupLocationToUse() : ((this.userService.getUserPickupLocation() && this.userService.getUserPickupLocation().length > 0) ? this.userService.getUserPickupLocation()[0] : {});
    public userPickupAddressToUseAddressId: string = "";
    public userTravelMode: string = (this.userService.getUserTransportmode() == 0) ? "walk" : "drive";
    public radius: number = this.userService.getUserRadiusMiles();
    public tmpRadius: number = this.userService.getUserRadiusMiles();;
    public selectedEstablishmentId: string = "";
    public breadcumbEstablishmentId: string = "";
    public breadcumbProductCategory: string = "";

    public latitude: number;
    public longitude: number;
    public formatted_address: string;
    public searchControl: FormControl;

    @ViewChild("pickup")
    public searchElementRef: ElementRef;

    constructor(
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private userService: UserService,
        private router: Router,
        private ProviderServiceService: ProviderServiceService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params.establishmentId) {
                this.breadcumbEstablishmentId = params.establishmentId;
            }
            if (params.productCategory) {
                this.breadcumbProductCategory = params.productCategory;
            }
        });
        //create search FormControl
        this.searchControl = new FormControl();
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.formatted_address = place.formatted_address

                    let userPickupAddress = [{
                        "geoLocation": {
                            "coordinates": [
                                place.geometry.location.lng(),
                                place.geometry.location.lat()
                            ],
                            "type": "Point"
                        },
                        "address": this.formatted_address
                    }];
                });
            });
        });

        this.userService.showAppSpinner();
        this.userPickupAddressToUse = (this.userService.getUserPickupLocationToUse()) ? this.userService.getUserPickupLocationToUse() : ((this.userService.getUserPickupLocation() && this.userService.getUserPickupLocation().length > 0) ? this.userService.getUserPickupLocation()[0] : {});
        if (Object.keys(this.userPickupAddressToUse).length > 0) {
            this.userPickupAddressToUseAddressId = this.userPickupAddressToUse['id'];
            this.userService.setUserPickupLocationToUse(this.userPickupAddressToUse);
            if (!this.userService.getUserRadiusMiles()) {
                this.userService.setUserRadiusMiles(5);
            }
            if (!this.userService.getUserTransportmode()) {
                this.userService.setUserTransportmode(0);
            }
            this.getStoreListing();
            // if (window.navigator && window.navigator.geolocation) {
            //     window.navigator.geolocation.getCurrentPosition(
            //         position => {
            //             if (!this.userService.getUserRadiusMiles()) {
            //                 this.userService.setUserRadiusMiles(5);
            //                 this.radius = 5;
            //             }
            //             if (!this.userService.getUserTransportmode()) {
            //                 this.userService.setUserTransportmode(0);
            //                 this.userTravelMode = '0';
            //             }

            //             this.userCurrentLocation = position;
            //             this.getStoreListing();
            //         },
            //         error => {
            //             if (!this.userService.getUserRadiusMiles()) {
            //                 this.userService.setUserRadiusMiles(5);
            //             }
            //             if (!this.userService.getUserTransportmode()) {
            //                 this.userService.setUserTransportmode(0);
            //             }
            //             let position: any = {
            //                 "latitude": 40.682570,
            //                 "longitude": -73.930284
            //             };
            //             this.userCurrentLocation = position;
            //             this.getStoreListing();
            //         }
            //     );
            // } else {
            //     console.log("GONE INTO ELSE");
            //     this.userService.hideAppSpinner();
            // }
        } else {
            this.router.navigate(['/nopickupaddress']);
        }
    }

    public getStoreListing() {
        this.ProviderServiceService.getStoreListing(this.userCurrentLocation, this.userPickupAddressToUse)
            .subscribe(data => {
                this.storeListing = this.allStoreListing = data;
                this.storeIds = [];
                this.storeListing.forEach((store) => {
                    this.storeIds.push(store.id);
                });
                if (this.storeIds.length > 0) {
                    this.ProviderServiceService.getProductListing(this.storeIds)
                        .subscribe(data => {
                            this.productListing = this.allProductListing = data;
                            if (data.length > 0) {
                                this.establishmentData = this.storeListing.filter(
                                    store => {
                                        return (data[0].establishment.id == store.id) ? true : false;
                                    }
                                );
                                if (this.establishmentData.length > 0) {
                                    this.establishmentData = this.establishmentData[0]
                                    this.establishmentData.coordinates = this.establishmentData.geoLocation.coordinates
                                }
                            }

                            if (this.breadcumbEstablishmentId == "") {
                                // $(".store_check.checkbox-square").eq(0).trigger("click");
                                $(".checkbox-design").eq(0).find('span').trigger("click")
                            } else {
                                $("input[data-establishment-id='" + this.breadcumbEstablishmentId + "']").trigger("click");
                            }
                            this.userService.hideAppSpinner();
                        });
                } else {
                    this.userService.hideAppSpinner();
                    // if (this.userService.isLoggedIn()) {
                    //     this.router.navigate(['/nostoresavailable']);
                    // }
                    this.router.navigate(['/nostoresavailable']);
                }
            });
    }

    public timeIncrement(time: string) {
        let newTime = 0.1 * (parseInt(time) + 1);
        return newTime + "s";
    }

    public filterToggle() {
        // var selector = $(args.currentTarget).data("target");
        // $(selector).toggleClass('in');
        // $('.filter-view').toggleClass('main');

        $("input[name='categoryFilter']").each((index, item) => {
            $(item).prop('checked', false);
        });
        this.categoryFilter(true);
    }

    public categoryFilter(args) {
        this.breadcumbProductCategory = "";
        this.productListing = this.allProductListing;
        this.productFilterArray = [];
        $("input[name='categoryFilter']").each((index, item) => {
            if ($(item).is(":checked")) {
                $(item).prop('checked', false);
            }
        });

        if (args !== true) {
            $(args.currentTarget).prop('checked', true);
            this.productFilterArray.push($(args.currentTarget).val());
        }


        if (this.productFilterArray.length > 0) {
            this.productListing = this.productListing.filter(
                product => {
                    return ($.inArray(product.product.category, this.productFilterArray) > -1) ? true : false;
                }
            );
        }
    }

    public storeFilter(args) {
        this.userService.showAppSpinner();
        this.selectedEstablishmentId = $(args.currentTarget).find('input').data("establishmentId");
        this.tempStoreListing = this.allStoreListing;
        this.storeFilterArray = [];
        $("input[name='stores']").each((index, item) => {
            if ($(item).is(":selected")) {
                this.storeFilterArray.push($(item).data("establishmentId"));
            }
        });
        if ($.inArray($(args.currentTarget).find('input').data("establishmentId"), this.storeFilterArray) != -1) {
            this.storeFilterArray.splice(this.storeFilterArray.indexOf($(args.currentTarget).find('input').data("establishmentId")), 1)
        } else {
            this.storeFilterArray.push($(args.currentTarget).find('input').data("establishmentId"));
        }

        if (this.storeFilterArray.length > 0) {
            this.tempStoreListing = this.tempStoreListing.filter(
                store => {
                    return ($.inArray(store.id, this.storeFilterArray) > -1) ? true : false;
                }
            );
        } else {
            this.storeFilterArray = this.storeIds;
        }
        this.ProviderServiceService.getProductListing(this.storeFilterArray)
            .subscribe(data => {
                this.productListing = data;
                this.establishmentData = this.storeListing.filter(
                    store => {
                        return (this.selectedEstablishmentId == store.id) ? true : false;
                    }
                );
                if (this.establishmentData.length > 0) {
                    this.establishmentData = this.establishmentData[0]
                    this.establishmentData.coordinates = this.establishmentData.geoLocation.coordinates
                }

                this.productFilterArray = [];
                if (this.breadcumbProductCategory == "") {
                    let categoryValue = $('[name="categoryFilter"]:checked').val()
                    if (typeof categoryValue !== "undefined") {
                        this.productFilterArray.push(categoryValue);
                    }


                    if (this.productFilterArray.length > 0) {
                        this.productListing = this.productListing.filter(
                            product => {
                                return ($.inArray(product.product.category, this.productFilterArray) > -1) ? true : false;
                            }
                        );
                    }
                } else {
                    $('[value="' + this.breadcumbProductCategory + '"]').trigger("click");
                }

                this.userService.hideAppSpinner();
            });

    }

    myOnChange(args) {
        this.tmpRadius = args.from;
        return false;
    }

    saveUserPickupLocation() {
        this.userService.showAppSpinner();
        if (this.userService.isLoggedIn()) {
            this.ProviderServiceService.updateUserProfile(this.tmpRadius, this.userService.getUserTransportmode())
                .subscribe(data => {
                    this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                        .subscribe(data => {
                            this.radius = this.tmpRadius;
                            this.userService.setCurrentUserData(data);
                        })
                })
        }
        if (this.latitude == null || this.longitude == null || this.tmpRadius == 0) {
            this.userService.hideAppSpinner();
            $('.pickupheader.profile-head .dropdown-toggle').attr('aria-expanded', "false").parent().removeClass('open');
        } else {
            let userPickupAddress = [{
                "geoLocation": {
                    "coordinates": [
                        this.longitude,
                        this.latitude
                    ],
                    "type": "Point"
                },
                "address": this.formatted_address
            }];
            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.addLocation(this.latitude, this.longitude)
                    .subscribe(data => {
                        this.ProviderServiceService.addUserPickupLocation(this.latitude, this.longitude, this.formatted_address, data.id)
                            .subscribe(data => {
                                this.ProviderServiceService.getUserPickupLocation()
                                    .subscribe(data => {
                                        this.radius = this.tmpRadius;
                                        this.userCurrentLocation = data;
                                        this.userService.setUserPickupLocation(data);
                                        this.userService.setUserPickupLocationToUse(data[0]);
                                        this.userPickupAddressToUse = data[0];
                                        this.userPickupAddressToUseAddressId = this.userPickupAddressToUse['id']
                                        $(".history li").eq(0).trigger("click");

                                        this.userService.hideAppSpinner();
                                        this.getStoreListing();
                                    });
                            },
                                error => {
                                    this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                                    this.userService.hideAppSpinner();
                                });
                    },
                        error => {
                            this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                            this.userService.hideAppSpinner();
                        });
            } else {
                this.radius = this.tmpRadius;
                this.userPickupAddress = userPickupAddress;
                this.userPickupAddressToUse = userPickupAddress[0];
                this.userPickupAddressToUseAddressId = this.userPickupAddressToUse['id'];

                this.userService.setUserPickupLocation(userPickupAddress);
                this.userService.setUserPickupLocationToUse(userPickupAddress[0]);
                this.userService.hideAppSpinner();
                this.getStoreListing();
            }
        }

    }

    changeAddress(args: any, id: string) {
        if ($("#history_" + id).is(':checked') === false) {
            $("#history_" + id).prop('checked', true);
        }
        if (args.target.nodeName == 'INPUT') return false;
        let selectedAddress = this.userPickupAddress.filter(
            address => {
                return (id == address.id) ? true : false;
            }
        );
        this.userPickupAddressToUse = selectedAddress[0];
        this.userPickupAddressToUseAddressId = this.userPickupAddressToUse['id'];
        this.userService.setUserPickupLocationToUse(selectedAddress[0]);
        this.getStoreListing();
    }

    showProductListingOnMobile() {
        $('#stores ul li').click(function () {
            $('.mobileview-productlisting').css(
                {
                    'transform': 'translate(0%)',
                    'transition': 'all linear 0.2s'
                }
            );
        });
        $('.close-productListing-btn').click(function () {
            $('.mobileview-productlisting').css(
                { 'transform': 'translate(100%)' }
            );
        });
    }
}
