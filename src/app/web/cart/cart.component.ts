import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';
import * as $ from "jquery";
@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public loggedInUserData: Array<any> = this.userService.getCurrentUserData();
    public cartItems: Array<any> = [];
    public establishmentData: Array<any> = [];
    public establishmentDistance: any;
    public establishmentId: string = "";
    public userCurrentLocation: any = {};
    public userTravelMode: string = (this.userService.getUserTransportmode() == 0) ? "walk" : "drive";
    public taxBasedOnEstablishment: number = 0;
    public showEarnPointsText: boolean = true;


    constructor(
        private route: ActivatedRoute,
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService
    ) {

    }

    ngOnInit() {
        this.userService.showAppSpinner();
        if (this.userService.isLoggedIn()) {
            this.ProviderServiceService.getUserDiscountInfo()
                .subscribe(data => {
                    this.userService.setDiscountInfo(data.discountAmount);
                    this.userService.setDiscountJson(data);
                    if (data.discountAmount > 0) {
                        this.showEarnPointsText = false;
                    }
                });
        }
        this.fetchUserShoppingCartData(() => {
            if (window.navigator && window.navigator.geolocation) {
                window.navigator.geolocation.getCurrentPosition(
                    position => {
                        this.userCurrentLocation = position.coords;
                        // this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.establishmentLatitude, this.establishmentLongitude)
                        if (this.establishmentId == "") {
                            this.userService.hideAppSpinner();
                        } else {
                            this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.userCurrentLocation.latitude, this.userCurrentLocation.longitude)
                                .subscribe(data => {
                                    this.establishmentDistance = data[0];
                                    this.userService.hideAppSpinner();
                                });
                        }
                    },
                    error => {
                        this.userCurrentLocation = {
                            "latitude": 40.682570,
                            "longitude": -73.930284
                        };
                        // this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.establishmentLatitude, this.establishmentLongitude)

                        if (this.establishmentId == "") {
                            this.userService.hideAppSpinner();
                        } else {
                            this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.userCurrentLocation.latitude, this.userCurrentLocation.longitude)
                                .subscribe(data => {
                                    this.establishmentDistance = data[0];
                                    this.userService.hideAppSpinner();
                                });
                        }
                    })
            }
        });
    }

    public fetchUserShoppingCartData(callback) {
        if (this.userService.isLoggedIn()) {
            this.ProviderServiceService.getShoppingCart()
                .subscribe(data => {
                    this.userService.setShoppingCartData(data);
                    this.userService.setShoppingCartCount(data.length);
                    this.cartItems = data;
                    if (data.length > 0) {
                        this.establishmentData = data[0].offer.establishment;
                        this.userService.setCartEstablishmentId(data[0].offer.establishmentId);

                        this.establishmentId = this.userService.getCartEstablishmentId();
                        this.ProviderServiceService.getEstablishmentDetails(this.establishmentId)
                            .subscribe(data => {
                                this.establishmentData = data;
                                this.ProviderServiceService.getTaxFromLatLong(data.geoLocation.coordinates[1], data.geoLocation.coordinates[0], response => {
                                    this.taxBasedOnEstablishment = parseFloat(response.combined_rate);
                                    this.userService.setTaxRate(response.combined_rate);
                                    this.refreshCart();
                                    callback();
                                });
                            });
                    } else {
                        callback();
                    }
                });
        } else {
            let cartLocalStorage = this.userService.getCartLocalStorage();
            this.cartItems = (cartLocalStorage) ? cartLocalStorage : [];
            if (this.cartItems.length > 0) {
                this.establishmentData = this.cartItems[0].offer.establishment;
                this.userService.setCartEstablishmentId(this.cartItems[0].offer.establishmentId);

                this.establishmentId = this.userService.getCartEstablishmentId();
                this.ProviderServiceService.getEstablishmentDetails(this.establishmentId)
                    .subscribe(data => {
                        this.establishmentData = data;
                        this.ProviderServiceService.getTaxFromLatLong(data.geoLocation.coordinates[1], data.geoLocation.coordinates[0], response => {
                            this.userService.setTaxRate(response.combined_rate);
                            this.taxBasedOnEstablishment = parseFloat(response.combined_rate);
                            this.refreshCart();
                            callback();
                        });
                    });
            } else {
                callback();
            }
        }
    }

    public removeFromCart(cartId: string) {
        if (confirm("Are you sure you want to remove this item? ")) {
            this.userService.showAppSpinner();
            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.removeItemShoppingCart(cartId)
                    .subscribe(data => {
                        if (data.count > 0) {
                            this.userService.showflashMessage("success", 'Item removed from cart successsfully');
                            this.ProviderServiceService.getShoppingCart()
                                .subscribe(data => {
                                    this.userService.setShoppingCartData(data);
                                    this.userService.setShoppingCartCount(data.length);
                                    this.cartItems = data;
                                    this.userService.setHeaderCartCheck('true');
                                    if (data.length > 0) {
                                        this.establishmentData = data[0].offer.establishment;
                                        this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                                    } else {
                                        this.userService.unsetCartEstablishmentId();
                                        this.userService.setDiscountInfo(0);
                                        this.userService.setDefaultDiscountJson();
                                    }
                                    this.userService.hideAppSpinner();
                                });
                        } else {
                            this.userService.showflashMessage("danger", 'Error occured while removing item', 5000);
                            this.userService.hideAppSpinner();
                        }
                    });
            } else {
                console.log("DELETE FROM LOCAL");
                this.deleteLocalCartData(cartId);
            }

        }
    }

    public updateCartQuantity(args, cartId: string) {
        this.userService.showAppSpinner();
        let lastVal = $(args.currentTarget).parents('.qty-input').find('.quantity-input').val();
        let newVal = 0;
        if ($(args.currentTarget).hasClass('more')) {
            newVal = parseInt(lastVal) + 1;
        } else if ($(args.currentTarget).hasClass('less')) {
            newVal = parseInt(lastVal) - 1;
        }
        if (newVal > 0) {
            if (this.userService.isLoggedIn()) {
                $(args.currentTarget).parents('.qty-input').find('.quantity-input').val(newVal);
                this.ProviderServiceService.updateShoppingCart(cartId, newVal)
                    .subscribe(data => {
                        this.ProviderServiceService.getShoppingCart()
                            .subscribe(data => {
                                this.userService.showflashMessage("success", 'Quantity Update Successfully');
                                this.userService.setShoppingCartData(data);
                                this.userService.setShoppingCartCount(data.length);
                                this.cartItems = data;
                                if (data.length > 0) {
                                    this.establishmentData = data[0].offer.establishment;
                                    this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                                } else {
                                    this.userService.unsetCartEstablishmentId();
                                }
                                this.userService.hideAppSpinner();
                            });
                    });
            } else {
                $(args.currentTarget).parents('.qty-input').find('.quantity-input').val(newVal);
                this.ProviderServiceService.getOfferDetails(cartId)
                    .subscribe(data => {
                        let cartLocalData: any = {};
                        cartLocalData.offer = data[0];
                        cartLocalData.product = data[0].product;
                        cartLocalData.quantity = newVal;
                        delete cartLocalData.offer.product;
                        this.changeLocalCartQuantity(cartLocalData);
                        this.userService.setHeaderCartCheck('true');
                        this.userService.hideAppSpinner();
                    });
            }

        } else {
            this.ProviderServiceService.removeItemShoppingCart(cartId)
                .subscribe(data => {
                    if (data.count > 0) {
                        this.userService.showflashMessage("success", 'Item removed from cart successsfully');
                        this.ProviderServiceService.getShoppingCart()
                            .subscribe(data => {
                                this.userService.setShoppingCartData(data);
                                this.userService.setShoppingCartCount(data.length);
                                this.cartItems = data;
                                if (data.length > 0) {
                                    this.establishmentData = data[0].offer.establishment;
                                    this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                                } else {
                                    this.userService.unsetCurrentEstablishmentId();
                                    this.userService.unsetCartEstablishmentId();
                                }
                                this.userService.hideAppSpinner();
                            });
                    } else {
                        this.userService.showflashMessage("danger", 'Error occured while removing item', 5000);
                    }
                });
        }
        this.userService.setHeaderCartCheck('true');
    }

    changeLocalCartQuantity(cartLocalData) {
        let localCartData = this.userService.getCartLocalStorage();
        let itemFound = false;
        localCartData.forEach(cartItem => {
            if (cartItem.offer.id == cartLocalData.offer.id && itemFound == false) {
                itemFound = true;
                cartItem.quantity = cartLocalData.quantity;
            }
        });
        if (!itemFound) {
            this.userService.setCartLocalStorage(cartLocalData);
        } else {
            this.userService.replaceCartLocalStorage(localCartData);
        }
        this.cartItems = this.userService.getCartLocalStorage();
    }

    deleteLocalCartData(offerId: string) {
        let localCartData = this.userService.getCartLocalStorage();
        let itemFound = false;

        let index = localCartData.length - 1;

        while (index >= 0) {
            if (localCartData[index].offer.id === offerId) {
                localCartData.splice(index, 1);
            }

            index -= 1;
        }

        this.userService.replaceCartLocalStorage(localCartData);
        this.cartItems = this.userService.getCartLocalStorage();
        this.userService.setHeaderCartCheck('true');
        this.userService.hideAppSpinner();
    }

    applyPromocode(args) {
        if (this.userService.isLoggedIn()) {
            let promocode: string = "";
            if ($(args.currentTarget).prev().val() != "") {
                promocode = $(args.currentTarget).prev().val();
            }
            this.ProviderServiceService.getUserDiscountInfo(promocode)
                .subscribe(data => {
                    this.userService.unsetDiscountInfo();
                    this.userService.setDefaultDiscountJson();
                    if (data.discountAmount == 0) {
                        if (data.discountUsed == "NA") {
                            this.showPromocodeError("No Discount Applied");
                        } else {
                            this.showPromocodeError(data.discountUsed);
                        }
                        this.userService.unsetDiscountInfo();
                    } else {
                        this.showPromocodeError("Promocode Applied", true);
                        this.userService.setDiscountInfo(data.discountAmount);
                        this.userService.setDiscountJson(data);
                        this.showEarnPointsText = false;
                    }
                    this.refreshCart();
                    this.userService.hideAppSpinner();
                });
        } else {
            this.showPromocodeError("Please login to apply promo code");
            this.userService.unsetDiscountInfo();
            this.userService.setDefaultDiscountJson();
            this.refreshCart();
            this.userService.hideAppSpinner();
        }
    }

    refreshCart() {
        if (this.userService.isLoggedIn()) {
            this.cartItems = JSON.parse(this.userService.getShoppingCartData());
            this.userService.setHeaderCartCheck('true');
        } else {
            this.cartItems = this.userService.getCartLocalStorage();
            this.userService.setHeaderCartCheck('true');
        }
    }

    showPromocodeError(message: string, success: boolean = false) {
        $('#promocodeError').text(message).removeClass('hidden');
        if (success == false) {
            $("#promocode").val("");
            let promocodeErrorTimer = setInterval(function () {
                clearInterval(promocodeErrorTimer);
                $('#promocodeError').text("").addClass('hidden');
            }, 5000);
        } else {

        }
    }

    clearPromocode() {

    }

}
