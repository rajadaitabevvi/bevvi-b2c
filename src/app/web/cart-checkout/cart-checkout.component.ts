import { Router } from '@angular/router';
import { UserService } from './../../shared/services/user.service';
import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { ElementRef, NgModule, ViewChild, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { Angulartics2Facebook } from 'angulartics2/facebook';
import * as $ from 'jquery';

@Component({
    selector: 'app-cart-checkout',
    templateUrl: './cart-checkout.component.html',
    styleUrls: ['./cart-checkout.component.css']
})
export class CartCheckoutComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public establishmentData: any = {};
    public establishmentId: string = "";
    public userCurrentLocation: any = {};
    public establishmentLatitude: number = 0;
    public establishmentLongitude: number = 0;
    public establishmentDistance: any;
    public zoom: number = 16;
    public currentUserDetails: any = (this.userService.isLoggedIn()) ? this.userService.getCurrentUserData() : {};
    public userPickupAddress: Array<any> = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
    public userTravelMode: string = (this.userService.getUserTransportmode() == 0) ? "walk" : "drive";
    public cartItems: Array<any> = JSON.parse(this.userService.getShoppingCartData());
    public content_ids: Array<any> = [];
    public totalPrice: number = 0;
    // public origin = { longitude: -73.930284, latitude: 40.682570 };  // its a example aleatory position
    // public destination = { longitude: 0, latitude: 0 };  // its a example aleatory position

    constructor(
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private router: Router,
        private Angulartics2Facebook: Angulartics2Facebook
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.cartItems.forEach((item) => {
            this.content_ids.push(item.offerId);
            this.totalPrice += item.offer.salePrice;
        })
        this.Angulartics2Facebook.eventTrack('InitiateCheckout', {
            content_ids: this.content_ids,
            content_type: 'product',
            value: this.totalPrice,
            currency: 'USD'
        });
        this.establishmentId = this.userService.getCartEstablishmentId();
        this.ProviderServiceService.getEstablishmentDetails(this.establishmentId)
            .subscribe(data => {
                this.establishmentData = data;
                // this.destination = { longitude: this.establishmentLongitude, latitude: this.establishmentLatitude };
                this.establishmentLatitude = this.establishmentData.geoLocation.coordinates[1];
                this.establishmentLongitude = this.establishmentData.geoLocation.coordinates[0];

                if (window.navigator && window.navigator.geolocation) {
                    window.navigator.geolocation.getCurrentPosition(
                        position => {
                            this.userCurrentLocation = position.coords;
                            // this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.establishmentLatitude, this.establishmentLongitude)
                            this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.userCurrentLocation.latitude, this.userCurrentLocation.longitude)
                                .subscribe(data => {
                                    this.establishmentDistance = data[0];
                                    this.userService.hideAppSpinner();
                                })
                        },
                        error => {
                            this.userCurrentLocation = {
                                "latitude": 40.682570,
                                "longitude": -73.930284
                            };
                            // this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.establishmentLatitude, this.establishmentLongitude)
                            this.ProviderServiceService.getEstablishmentDistance(this.establishmentId, this.userCurrentLocation.latitude, this.userCurrentLocation.longitude)
                                .subscribe(data => {
                                    this.establishmentDistance = data[0];
                                    this.userService.hideAppSpinner();
                                })
                        })
                }
            });
    }

    cart_step1_next() {
        if ($("#dateForServer").val() == "") {
            this.userService.showflashMessage("danger", "Please select Pickup Datetime before moving forward");
        } else {
            let userPickupDatetime = $("#dateForServer").val();
            this.userService.setUserPickupDateTime(userPickupDatetime);
            this.router.navigate(['/cart-payment']);
        }
    }

}
