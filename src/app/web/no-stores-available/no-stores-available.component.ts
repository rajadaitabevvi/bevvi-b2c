import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';

@Component({
    selector: 'app-no-stores-available',
    templateUrl: './no-stores-available.component.html',
    styleUrls: ['./no-stores-available.component.css']
})
export class NoStoresAvailableComponent implements OnInit {

    public userPickupAddress: Array<any> = [];
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public loggedInUserData = (this.isUserLoggedIn) ? this.userService.getCurrentUserData() : []
    public addressToShow: string;
    public showPastAddresses: boolean = false;

    constructor(
        private userService: UserService,
        private router: Router,
        private ProviderServiceService: ProviderServiceService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        if (!this.isUserLoggedIn) {
            this.userPickupAddress = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
            this.addressToShow = this.userService.getUserPickupLocationToUse().address;
            this.userPickupAddress = [];
            this.userService.unsetUserPickupLocationToUse();
            this.userService.unsetUserPickupLocation();
        } else {
            this.userPickupAddress = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
            this.addressToShow = this.userService.getUserPickupLocationToUse().address;
            if (this.userPickupAddress.length > 1) {
                this.showPastAddresses = true;
            }
        }
    }

    bringBevviToCity(address: string) {
        this.userService.showAppSpinner();
        let email = $("input[name='userEmail']").val();
        if (email == "") {
            alert("Email address is required");
            this.userService.hideAppSpinner();
        } else {
            this.ProviderServiceService.bringBevviToCity(email, address)
                .subscribe(data => {
                    $("#thankYouText").removeClass("hidden");
                    $("#emailDiv, #submitButton").addClass("hidden");
                    this.userService.hideAppSpinner();
                }, error => {
                    this.userService.hideAppSpinner();
                })
        }
    }

    changeAddress($event, id) {
        let selectedAddress = this.userPickupAddress.filter(
            address => {
                return (id == address.id) ? true : false;
            }
        );
        this.userService.setUserPickupLocationToUse(selectedAddress[0]);
        this.router.navigate(['/productlisting']);
    }

}
