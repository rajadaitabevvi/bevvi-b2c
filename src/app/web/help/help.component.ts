import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
    selector: 'app-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {

    element: any;
    hash: string = "";
    constructor() { }

    ngOnInit() {
    }

    scrollToElement(id) {
        this.element = document.querySelector("#" + id);
        if (this.element) {
            this.hash = "#" + id;
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 130
                }, 800);
            }

        }
    }

}
