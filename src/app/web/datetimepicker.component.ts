import { forwardRef, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, Component } from '@angular/core';

declare var jQuery: any;

@Component({
    selector: 'my-datepicker',
    template: `<input style="width:100%; font-size:17.5px;" id="datetimepicker" #input type="text" class="form-control1 mar-bot20" placeholder="Choose Pickup Date / Time">`
})
export class DatetimepickerComponent implements AfterViewInit {
    @ViewChild('input') input: ElementRef;
    public datepickerCheckInterval: any;
    ngAfterViewInit() {

        this.datepickerCheckInterval = setInterval(() => {
            if (typeof jQuery.datetimepicker != "undefined") {
                clearInterval(this.datepickerCheckInterval);

                jQuery(this.input.nativeElement).datetimepicker({
                    format: 'F d, Y l h:i A',
                    validateOnBlur: false,
                    forceParse: false,
                    minDate: 0,
                    onClose: function (current_time, $input) {
                        $("#dateForServer").val(new Date(current_time).toISOString());
                    }
                });
            }
        }, 10);
    }
}