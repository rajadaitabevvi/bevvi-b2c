import { Router } from '@angular/router';
import { UserService } from './../../shared/services/user.service';
import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { ElementRef, NgModule, ViewChild } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { Angulartics2Facebook } from 'angulartics2/facebook';
import * as $ from 'jquery';

@Component({
    selector: 'app-cart-checkout-summary',
    templateUrl: './cart-checkout-summary.component.html',
    styleUrls: ['./cart-checkout-summary.component.css']
})
export class CartCheckoutSummaryComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public userSavedCards: Array<any> = [];
    public userPickupTime: string = this.userService.getUserPickupDateTime();
    public establishmentData: any = {};
    public establishmentId: string = "";
    public establishmentLatitude: number = 0;
    public establishmentLongitude: number = 0;
    public zoom: number = 16;
    public currentUserDetails: any = (this.userService.isLoggedIn()) ? this.userService.getCurrentUserData() : {};
    public userPickupAddress: Array<any> = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
    public userTravelMode: string = (this.userService.getUserTransportmode() == 0) ? "walk" : "drive";
    public cartItems: Array<any> = [];
    public placeOrderIsDisabled: boolean = false;
    public content_ids: Array<any> = [];
    public totalPrice: number = 0;

    constructor(
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private router: Router,
        private Angulartics2Facebook: Angulartics2Facebook
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.fetchUserCards();
        this.establishmentId = this.userService.getCartEstablishmentId();
        this.ProviderServiceService.getEstablishmentDetails(this.establishmentId)
            .subscribe(data => {
                this.establishmentData = data;
                this.establishmentLatitude = this.establishmentData.geoLocation.coordinates[1];
                this.establishmentLongitude = this.establishmentData.geoLocation.coordinates[0];
                this.ProviderServiceService.getShoppingCart()
                    .subscribe(data => {
                        this.cartItems = data;
                        this.userService.hideAppSpinner();
                    });
            });

    }

    fetchUserCards() {
        this.ProviderServiceService.getUserSavedCards(data => {
            if (data.sources.data.length > 0) {
                this.userSavedCards = data.sources.data.filter(
                    card => {
                        return (this.userService.getUserPaymentCard() == card.id) ? true : false;
                    }
                );
                this.userSavedCards = this.userSavedCards[0];
            }
        });
    }

    public removeFromCart(cartId: string) {
        if (confirm("Are you sure you want to remove this item? ")) {
            this.userService.showAppSpinner();
            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.removeItemShoppingCart(cartId)
                    .subscribe(data => {
                        if (data.count > 0) {
                            this.ProviderServiceService.getShoppingCart()
                                .subscribe(data => {
                                    this.userService.setShoppingCartData(data);
                                    this.userService.setShoppingCartCount(data.length);
                                    this.cartItems = data;
                                    this.userService.setHeaderCartCheck('true');
                                    if (data.length > 0) {
                                        this.establishmentData = data[0].offer.establishment;
                                        this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                                    } else {
                                        this.userService.unsetCartEstablishmentId();
                                        this.userService.unsetDiscountInfo();
                                    }
                                    this.userService.hideAppSpinner();
                                });
                        }
                    });
            } else {
                console.log("DELETE FROM LOCAL");
            }

        }
    }

    public updateCartQuantity(args, cartId: string) {
        this.userService.showAppSpinner();
        $('tr[data-cartId=' + cartId + ']').removeClass('notAvailable');
        let lastVal = $(args.currentTarget).parents('.qty-input').find('.quantity-input').val();
        let newVal = 0;
        if ($(args.currentTarget).hasClass('more')) {
            newVal = parseInt(lastVal) + 1;
        } else if ($(args.currentTarget).hasClass('less')) {
            newVal = parseInt(lastVal) - 1;
        }
        if (newVal > 0) {
            $(args.currentTarget).parents('.qty-input').find('.quantity-input').val(newVal);
            this.ProviderServiceService.updateShoppingCart(cartId, newVal)
                .subscribe(data => {
                    this.ProviderServiceService.getShoppingCart()
                        .subscribe(data => {
                            this.userService.setShoppingCartData(data);
                            this.userService.setShoppingCartCount(data.length);
                            this.cartItems = data;
                            if (data.length > 0) {
                                this.establishmentData = data[0].offer.establishment;
                                this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                            } else {
                                this.userService.unsetCartEstablishmentId();
                                this.userService.unsetDiscountInfo();
                            }
                            this.userService.setHeaderCartCheck('true');
                            this.userService.hideAppSpinner();
                        });
                });
        } else {
            this.ProviderServiceService.removeItemShoppingCart(cartId)
                .subscribe(data => {
                    if (data.count > 0) {
                        this.ProviderServiceService.getShoppingCart()
                            .subscribe(data => {
                                this.userService.setShoppingCartData(data);
                                this.userService.setShoppingCartCount(data.length);
                                this.cartItems = data;
                                if (data.length > 0) {
                                    this.establishmentData = data[0].offer.establishment;
                                    this.userService.setCartEstablishmentId(data[0].offer.establishmentId);
                                } else {
                                    this.userService.unsetCurrentEstablishmentId();
                                    this.userService.unsetCartEstablishmentId();
                                    this.userService.unsetDiscountInfo();
                                }
                                this.userService.setHeaderCartCheck('true');
                                this.userService.hideAppSpinner();
                            });
                    } else {
                        this.userService.setHeaderCartCheck('true');
                        this.userService.hideAppSpinner();
                    }
                });
        }
    }

    public place_order() {
        this.userService.showAppSpinner();
        if (confirm('You will be charged $' + parseFloat($("#hiddentnuoma").val()).toFixed(2) + ' for this order. Do you want to continue?')) {
            this.ProviderServiceService.preCheckoutShoppingCart()
                .subscribe(data => {
                    if (data.notavailable.length > 0) {
                        window.scrollTo(0, 0);
                        this.userService.showflashMessage('danger', "Highlighted items are not available in required quantity");
                        data.notavailable.forEach(element => {
                            $('tr[data-cartId=' + element.id + ']').addClass('notAvailable');
                        });
                        this.userService.hideAppSpinner();
                    } else if (data.notavailable.length == 0) {
                        $('tr').removeClass('notAvailable');
                        this.placeOrderIsDisabled = true;

                        let finalDetails: any = {
                            amount: parseInt(($("#hiddentnuoma").val() * 100).toFixed(2)),
                            customerId: this.userService.getCurrentUserPayment(),
                            source: this.userService.getUserPaymentCard(),
                            pickupDateTime: this.userService.getUserPickupDateTime(),
                            accountId: this.userService.getCurrentAccountId(),
                            accessToken: this.userService.getAccessToken(),
                            taxAmount: this.userService.getTaxPrice(),
                            taxPercent: this.userService.getTaxRate(),
                            establishmentId: this.userService.getCartEstablishmentId(),
                            discountApplied: this.userService.getDiscountJson()
                        }
                        this.ProviderServiceService.getEstablishmentStripeAccount(this.userService.getCartEstablishmentId())
                            .subscribe(data => {
                                if (data.length > 0 && data[0].bizaccount && data[0].bizaccount.paymentIds && data[0].bizaccount.paymentIds[0] && data[0].bizaccount.paymentIds[0].token) {
                                    finalDetails.destination_account = data[0].bizaccount.paymentIds[0].token;
                                    this.ProviderServiceService.create_charge(finalDetails, response => {
                                        if (response.status == "error") {
                                            window.scrollTo(0, 0);
                                            this.placeOrderIsDisabled = false;
                                            if (response.discountUsed) {
                                                this.userService.showflashMessage("danger", response.discountUsed);
                                            } else if (response.message == "Authorization Required" || response.message == "Invalid Access Token") {
                                                this.userService.setLoginPopupCheck('true');
                                            }
                                            this.userService.hideAppSpinner();
                                        } else {
                                            this.cartItems.forEach((item) => {
                                                this.content_ids.push(item.offerId);
                                                this.totalPrice += item.offer.salePrice;
                                            })
                                            this.Angulartics2Facebook.eventTrack('Purchase', {
                                                content_ids: this.content_ids,
                                                content_type: 'product',
                                                value: this.totalPrice,
                                                currency: 'USD'
                                            });
                                            this.userService.hideAppSpinner();
                                            this.userService.afterCheckout();
                                            this.router.navigate(["/order-confirmation", response[0].id]);
                                        }
                                    });
                                } else {
                                    window.scrollTo(0, 0);
                                    this.userService.showflashMessage('danger', "Store doesn't have a Payment Account Setup yet. Please try ordering from a different store.");
                                    this.userService.hideAppSpinner();
                                }

                            });
                    } else {
                        window.scrollTo(0, 0);
                        this.userService.showflashMessage('danger', "Error occured. Please try again");
                        this.userService.hideAppSpinner();
                    }
                }, error => {
                    if (error.statusText == "Unauthorized") {
                        this.userService.setLoginPopupCheck('true');
                    }
                    this.userService.hideAppSpinner();
                });
        } else {
            this.placeOrderIsDisabled = false;
            this.userService.hideAppSpinner();
        }
    }

}
