import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-privacy-policy',
    templateUrl: './privacy-policy.component.html',
    styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

    constructor(
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        // if (this.route.snapshot.queryParams["fromMobile"]) {
        $('header, footer').addClass('hidden');
        $('.profile-head').css("margin-top", "0px");
        // }
    }

}
