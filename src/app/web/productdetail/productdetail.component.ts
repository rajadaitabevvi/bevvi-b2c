import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';
import { Angulartics2Facebook } from 'angulartics2/facebook'
import * as $ from "jquery";

@Component({
    selector: 'app-productdetail',
    templateUrl: './productdetail.component.html',
    styleUrls: ['./productdetail.component.css']
})

export class ProductdetailComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public loggedinUserData: object = {};
    public productData: any = {};
    public establishmentData: Array<any> = [];
    public currentofferId: string = "";
    public offerData: Array<any> = [];
    public addToCartObj: any = {};
    public cartQuantity: number = 0;
    public userATCBtn: Array<any>;
    private isInputDisabled: boolean = false;
    private isQuantityLessDisabled: boolean = false;
    private isQuantityMoreDisabled: boolean = false;
    public userPickupAddress: Array<any> = (this.userService.getUserPickupLocation()) ? this.userService.getUserPickupLocation() : [];
    public radius: number = this.userService.getUserRadiusMiles();

    constructor(
        private route: ActivatedRoute,
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private Angulartics2Facebook: Angulartics2Facebook
    ) {
        this.userService.showAppSpinner();
        this.route.params
            .subscribe(params => {
                this.currentofferId = params.offerId;
                this.ProviderServiceService.getOfferDetails(params.offerId)
                    .subscribe(data => {
                        $(".form-control.text-qty-input1").val(1);
                        this.productData = data[0].product;
                        this.establishmentData = data[0].establishment;
                        this.offerData = data[0];
                        this.userService.setCurrentEstablishmentId(this.establishmentData['id']);
                        this.addToCartObj = {
                            "accountId": this.userService.getCurrentAccountId(),
                            "productId": this.productData['id'],
                            "offerId": this.offerData['id']
                        }
                        this.Angulartics2Facebook.eventTrack('ViewContent', {
                            content_name: this.productData['name'],
                            content_category: this.productData['category'],
                            content_ids: [this.offerData['id']],
                            content_type: 'product',
                            value: this.offerData['salePrice'],
                            currency: 'USD'
                        });
                        this.userService.hideAppSpinner();
                    });
            });
    }

    ngOnInit() {
        if (this.isUserLoggedIn) {
            this.loggedinUserData = this.userService.getCurrentUserData();
            console.log(this.loggedinUserData);
        }
    }

    public changeQuantity(args) {
        let val = parseInt($('.qty-input1 input').val());

        if ($(args.currentTarget).hasClass('less')) {
            --val;
        } else if ($(args.currentTarget).hasClass('more')) {
            ++val;
        }

        if (val < 1) {
            val = 1;
        } else if (val > this.offerData['remQty']) {
            val = this.offerData['remQty'];
        }

        this.checkQuantity(val);
        $('.qty-input1 input').val(val);
    }

    public checkQuantity(currentVal: number) {
        if (currentVal == this.offerData['remQty']) {
            // $('button.btn.more').attr("disabled", 'true');
            // $('button.btn.less').attr("disabled", 'false');
            this.isQuantityLessDisabled = false;
            this.isQuantityMoreDisabled = true;
        } else {
            this.isQuantityLessDisabled = false;
            this.isQuantityMoreDisabled = false;
            // $('button.btn.more').attr("disabled", 'false');
            // $('button.btn.less').attr("disabled", 'false');
        }
    }

    public addToShoppingCart(args) {
        this.userService.showAppSpinner();
        this.userATCBtn = args.currentTarget;
        if (
            (this.userService.getCartEstablishmentId() != null && (this.userService.getCurrentEstablishmentId() == this.userService.getCartEstablishmentId()))
            || this.userService.getCartEstablishmentId() == null
        ) {
            let cartData = this.addToCartObj;
            cartData.quantity = $("input[name='quantity']").val();

            this.Angulartics2Facebook.eventTrack('AddToCart', {
                content_name: this.productData['name'],
                content_category: this.productData['category'],
                content_ids: [this.offerData['id']],
                content_type: 'product',
                value: this.offerData['salePrice'],
                currency: 'USD'
            });

            if ($(args.currentTarget).text() == "Add to Cart") {
                $(args.currentTarget).text("Added")
                $(args.currentTarget).removeClass("atc-btn").addClass("atc-btn1");
                this.isInputDisabled = true;
                setTimeout(data => {
                    this.isInputDisabled = false;
                    $(this.userATCBtn).text("Update Cart")
                    $(this.userATCBtn).removeClass("atc-btn1").addClass("atc-btn");
                }, 3000)
            } else if ($(args.currentTarget).text() == "Update Cart") {
                $(args.currentTarget).text("Updated")
                $(args.currentTarget).removeClass("atc-btn").addClass("atc-btn1");
                this.isInputDisabled = true;
                setTimeout(data => {
                    this.isInputDisabled = false;
                    $(this.userATCBtn).text("Update Cart")
                    $(this.userATCBtn).removeClass("atc-btn1").addClass("atc-btn");
                }, 3000)
            }

            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.addToShoppingCart(cartData)
                    .subscribe(data => {
                        this.ProviderServiceService.getShoppingCart()
                            .subscribe(data => {
                                this.userService.setShoppingCartData(data);
                                this.ProviderServiceService.getShoppingCartCount()
                                    .subscribe(data => {
                                        this.userService.setShoppingCartCount(data.count);
                                        this.userService.setHeaderCartCheck('true');
                                        this.userService.hideAppSpinner();
                                    });
                            });
                    });
            } else {
                this.ProviderServiceService.getOfferDetails(cartData.offerId)
                    .subscribe(data => {
                        let cartLocalData: any = {};
                        cartLocalData.offer = data[0];
                        cartLocalData.product = data[0].product;
                        cartLocalData.quantity = cartData.quantity;
                        delete cartLocalData.offer.product;
                        this.changeLocalCartQuantity(cartLocalData);
                        this.userService.setHeaderCartCheck('true');
                        this.userService.hideAppSpinner();
                    });
            }
        } else {
            console.log("PRODUCT FROM ANOTHER ESTABLISHMENT");
            alert("PRODUCT FROM ANOTHER ESTABLISHMENT");
            this.userService.hideAppSpinner();
        }
    }

    changeLocalCartQuantity(cartLocalData) {
        let localCartData = this.userService.getCartLocalStorage();
        let itemFound = false;
        localCartData.forEach(cartItem => {
            if (cartItem.offer.id == cartLocalData.offer.id && itemFound == false) {
                itemFound = true;
                cartItem.quantity = cartLocalData.quantity;
            }
        });
        if (!itemFound) {
            this.userService.setCartLocalStorage(cartLocalData);
        } else {
            this.userService.replaceCartLocalStorage(localCartData);
        }
    }

    myOnChange(args) {
        this.radius = args.from;
    }
}
