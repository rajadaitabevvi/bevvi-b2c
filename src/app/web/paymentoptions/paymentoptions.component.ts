import { UserService } from './../../shared/services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-paymentoptions',
    templateUrl: './paymentoptions.component.html',
    styleUrls: ['./paymentoptions.component.css']
})
export class PaymentoptionsComponent implements OnInit {
    public userSavedCards: Array<any> = [];
    constructor(
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.fetchUserCards();
    }

    fetchUserCards() {
        this.ProviderServiceService.getUserSavedCards(data => {
            this.userSavedCards = data.sources.data;
            this.userService.hideAppSpinner();
        });
    }

    deleteCard(sourceId: string) {
        if (confirm("Are you sure you want to remove this card?")) {
            this.userService.showAppSpinner();
            this.ProviderServiceService.deleteUserSavedCards(sourceId, data => {
                this.fetchUserCards();
                this.userService.showflashMessage("success", "Card deleted successfully");
            });
        }
    }

}
