import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { Router } from '@angular/router';
import { UserService } from './../../shared/services/user.service';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
    selector: 'app-no-address-available',
    templateUrl: './no-address-available.component.html',
    styleUrls: ['./no-address-available.component.css']
})
export class NoAddressAvailableComponent implements OnInit {
    public latitude: number;
    public longitude: number;
    public formatted_address: string;
    public radius: number = 0;
    public searchControl: FormControl;

    @ViewChild("search")
    public searchElementRef: ElementRef;

    constructor(
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private userService: UserService,
        private router: Router,
        private ProviderServiceService: ProviderServiceService
    ) { }

    ngOnInit() {
        this.userService.hideAppSpinner();
        //create search FormControl
        this.searchControl = new FormControl();
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.formatted_address = place.formatted_address

                    let userPickupAddress = [{
                        "geoLocation": {
                            "coordinates": [
                                place.geometry.location.lng(),
                                place.geometry.location.lat()
                            ],
                            "type": "Point"
                        },
                        "address": this.formatted_address
                    }];
                });
            });
        });
    }

    onSave() {
        this.userService.showAppSpinner();
        if (this.latitude == null || this.longitude == null) {
            this.userService.showflashMessage("danger", "Please select a proper location from the autocomplete dropdown");
            this.userService.hideAppSpinner();
        } else if (this.radius == 0) {
            this.userService.showflashMessage("danger", "Please specify Radius to see best deals near you");
            this.userService.hideAppSpinner();
        } else {
            let userPickupAddress = [{
                "geoLocation": {
                    "coordinates": [
                        this.longitude,
                        this.latitude
                    ],
                    "type": "Point"
                },
                "address": this.formatted_address
            }];
            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.addLocation(this.latitude, this.longitude)
                    .subscribe(data => {
                        this.ProviderServiceService.addUserPickupLocation(this.latitude, this.longitude, this.formatted_address, data.id)
                            .subscribe(data => {
                                this.ProviderServiceService.getUserPickupLocation()
                                    .subscribe(data => {
                                        this.userService.setUserPickupLocation(data);
                                        this.ProviderServiceService.updateUserProfile(this.radius, 0)
                                            .subscribe(data => {
                                                this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                                                    .subscribe(data => {
                                                        this.userService.setCurrentUserData(data);
                                                    })
                                            })
                                        this.userService.hideAppSpinner();
                                        this.router.navigate(['/productlisting']);
                                    });
                            },
                                error => {
                                    this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                                    this.userService.hideAppSpinner();
                                });
                    },
                        error => {
                            this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                            this.userService.hideAppSpinner();
                        });
            } else {
                this.userService.setUserPickupLocation(userPickupAddress);
                this.userService.setUserRadiusMiles(this.radius);
                this.userService.setUserTransportmode(0);
                this.userService.hideAppSpinner();
                this.router.navigate(['/productlisting']);
            }
        }
    }

    myOnChange(args) {
        this.radius = args.from;
    }

}
