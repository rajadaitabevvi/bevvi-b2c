import { Router } from '@angular/router';
import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { UserService } from './../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { ElementRef, NgModule, ViewChild } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';

@Component({
    selector: 'app-cart-checkout-payment',
    templateUrl: './cart-checkout-payment.component.html',
    styleUrls: ['./cart-checkout-payment.component.css']
})
export class CartCheckoutPaymentComponent implements OnInit {
    public userSavedCards: Array<any> = [];
    public userPickupTime: string = this.userService.getUserPickupDateTime();
    public establishmentId: string;
    public establishmentLatitude: number = 0;
    public establishmentLongitude: number = 0;
    public establishmentDistance: any;
    public zoom: number = 14;
    public establishmentData: any = {};
    public d = new Date();
    public currentYear = this.d.getFullYear();
    public currentMonth = this.d.getMonth();
    public isAddNewCard: boolean = false;

    constructor(
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.fetchUserCards();
        this.establishmentId = this.userService.getCartEstablishmentId();
        this.ProviderServiceService.getEstablishmentDetails(this.establishmentId)
            .subscribe(data => {
                this.establishmentData = data;
                this.establishmentLatitude = this.establishmentData.geoLocation.coordinates[1];
                this.establishmentLongitude = this.establishmentData.geoLocation.coordinates[0];
            });
    }

    fetchUserCards() {
        this.ProviderServiceService.getUserSavedCards(data => {
            this.userSavedCards = data.sources.data;
            if (data.sources.data.length > 0) {
                this.userService.setUserPaymentCard(data.sources.data[0].id);
            }
            this.userService.hideAppSpinner();
        });
    }

    deleteCard(sourceId: string) {
        this.userService.showAppSpinner();
        console.info(sourceId);
        if (confirm("Are you sure you want to remove this card?")) {
            this.ProviderServiceService.deleteUserSavedCards(sourceId, data => {
                this.fetchUserCards();
                this.userService.showflashMessage("success", "Card deleted successfully");
                this.userService.hideAppSpinner();
            });
        }
    }

    selectCard(args) {
        if ($(args.target).hasClass("delete")) {
            return false;
        }
        $(".col-md-7 .panel.panel-default").each((index, element) => {
            $(element).removeClass('bg-orange');
        });
        $(args.currentTarget).addClass('bg-orange');
        this.userService.setUserPaymentCard($(args.currentTarget).find('.panel-heading').data('src'));
    }

    addNewCard() {
        this.isAddNewCard = true;
    }

    showCardListing() {
        this.isAddNewCard = false;
    }

    saveCardDetailsToStripe() {
        this.userService.showAppSpinner();
        let isError = false;
        let cardDetail = {
            name: $("[name='name']").val(),
            number: $("[name='number']").val(),
            exp_month: $("[name='month']").val().split(' / ')[0],
            exp_year: $("[name='month']").val().split(' / ')[1],
            zipcode: $("[name='zip']").val(),
            cvc: $("[name='cvv']").val()
        }

        //Month Validation
        if (cardDetail.exp_month == "") {
            this.userService.showflashMessage("danger", "Expiry Month is required");
            isError = true;
        } else {
            if (isNaN(cardDetail.exp_month)) {
                this.userService.showflashMessage("danger", "Expiry month should be a valid number");
                isError = true;
            } else if (cardDetail.exp_month < 1 || cardDetail.exp_month > 12) {
                this.userService.showflashMessage("danger", "Expiry Month should be 0 and 12");
                isError = true;
            }
        }


        //Year Validation
        if (cardDetail.exp_year == "") {
            this.userService.showflashMessage("danger", "Expiry Year is requried");
            isError = true;
        } else if (!cardDetail.exp_year || cardDetail.exp_year.length < 4 || isNaN(cardDetail.exp_month)) {
            this.userService.showflashMessage("danger", "Expiry Year should be a valid 4-digit number");
            isError = true;
        } else if (cardDetail.exp_year == this.currentYear && cardDetail.exp_month < this.currentMonth) {
            this.userService.showflashMessage("danger", "Card is already expired");
            isError = true;
        } else if (cardDetail.exp_year < this.currentYear) {
            this.userService.showflashMessage("danger", "Expiry Year should be greater than current year");
            isError = true;
        }

        //CVV Validation
        if (cardDetail.cvc == "") {
            this.userService.showflashMessage("danger", "CVV is required");
            isError = true;
        } else if (cardDetail.cvc.length < 3 || isNaN(cardDetail.cvc)) {
            this.userService.showflashMessage("danger", "CVV should be valid 3-digit number");
            isError = true;
        }

        if (isError) {
            window.scrollTo(0, 0);
            this.userService.hideAppSpinner();
            return true;
        } else {
            this.ProviderServiceService.addUserSavedCards(cardDetail, response => {
                if (response.id) {
                    this.fetchUserCards();
                    this.isAddNewCard = false;
                    this.userService.showflashMessage("success", "Card added successfully");
                    this.userService.hideAppSpinner();
                } else {
                    this.userService.showflashMessage('danger', response.raw.message);
                    this.userService.hideAppSpinner();
                }
            });
        }
    }

    goToPlaceOrder() {
        if ($('.bg-orange').length == 1) {
            this.router.navigate(['/cart-checkout-summary']);
        } else {
            window.scroll(0, 0);
            this.userService.showflashMessage("danger", "Please select a card for payment or add a new one before proceeding");
            return false;
        }
    }

}
