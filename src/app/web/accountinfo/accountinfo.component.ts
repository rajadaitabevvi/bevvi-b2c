import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { UserService } from '../../shared/services/user.service';
import * as $ from "jquery";

@Component({
    selector: 'app-accountinfo',
    templateUrl: './accountinfo.component.html',
    styleUrls: ['./accountinfo.component.css']
})
export class AccountinfoComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public loggedInUserData: Array<any> = [];
    public phoneNumberModel: string = "";
    public usernameModel: string = "";

    accountform: FormGroup;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService
    ) {
        this.initForm();
    }

    initForm() {
        this.accountform = this.fb.group({
            password: [''],
            confirm_password: [''],
            phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
            username: ['', [Validators.required]],
        }, { validator: this.userService.checkIfMatchingPasswords('password', 'confirm_password') })
    }

    public onSaveProfile() {
        this.userService.showAppSpinner();
        let accountData: Object = {
            "firstName": this.accountform.get('username').value.split(' ')[0],
            "lastName": (this.accountform.get('username').value.split(' ')[1]) ? this.accountform.get('username').value.split(' ')[1] : '',
            "displayName": this.accountform.get('username').value,
            "phoneNumber": this.accountform.get('phoneNumber').value
        };
        if (this.accountform.get('password').value != "") {
            accountData['password'] = this.accountform.get('password').value;
        }
        this.ProviderServiceService.updateProfile(accountData)
            .subscribe(data => {
                this.userService.hideAppSpinner();
                this.userService.showflashMessage("success", "Account details saved successfully")
                this.userService.setCurrentUserData(data);
            }, error => {
                if (error.statusText == "Unauthorized") {
                    this.userService.setLoginPopupCheck('true');
                }
            });
    }

    get password() { return this.accountform.get('password'); }

    get confirm_password() { return this.accountform.get('confirm_password'); }

    get phoneNumber() { return this.accountform.get('phoneNumber'); }

    get username() { return this.accountform.get('username'); }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
            .subscribe(data => {
                this.userService.setCurrentUserData(data);
                this.phoneNumberModel = data.phoneNumber;
                this.usernameModel = (data.firstName ? data.firstName : '') + ' ' + (data.lastName ? data.lastName : '');
                this.userService.hideAppSpinner();
            }, error => {
                if (error.statusText == "Unauthorized") {
                    this.userService.setLoginPopupCheck('true');
                }
                this.userService.hideAppSpinner();
            });
    }

    ngAfterContentChecked() {
        this.loggedInUserData = (this.isUserLoggedIn) ? this.userService.getCurrentUserData() : []
        this.isUserLoggedIn = this.userService.isLoggedIn();
    }
}
