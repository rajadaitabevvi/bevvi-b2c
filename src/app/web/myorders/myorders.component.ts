import { CallbackPipe } from './../../shared/pipes/callback.pipe';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProviderServiceService } from './../../shared/services/provider-service.service';
import { UserService } from './../../shared/services/user.service';

@Component({
    selector: 'app-myorders',
    templateUrl: './myorders.component.html',
    styleUrls: ['./myorders.component.css'],
    providers: [CallbackPipe]
})
export class MyordersComponent implements OnInit {
    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public loggedInUserData: Array<any> = [];
    public userPickupAddress: Array<any> = [];
    public userOrders: Array<any> = [];

    constructor(
        private route: ActivatedRoute,
        private ProviderServiceService: ProviderServiceService,
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userService.showAppSpinner();
        this.ProviderServiceService.getUserOrders()
            .subscribe(data => {
                this.userOrders = data;
                this.userService.setUserOrders(data);
                this.userService.hideAppSpinner();
            });
    }

    ngAfterContentChecked() {
        this.loggedInUserData = (this.isUserLoggedIn) ? this.userService.getCurrentUserData() : []
        this.isUserLoggedIn = this.userService.isLoggedIn();
    }

    public filterpending_pickup(order: any) {
        return order.status == 1;
    }

    public filterpending_acceptance(order: any) {
        return order.status == 0;
    }

    public filterpast_orders(order: any) {
        return order.status == 2;
    }

    public filtercancelled_orders(order: any) {
        return order.status == 4;
    }

    public filterrejected_orders(order: any) {
        return order.status == 3;
    }

    public filterrejectedId_orders(order: any) {
        return order.status == 5;
    }
}
