import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProviderServiceService } from '../shared/services/provider-service.service';
import { Angulartics2 } from 'angulartics2';
import { BRANCH_LINK } from '../constants/constants';

@Component({
    selector: 'app-frontend',
    templateUrl: './frontend.component.html',
    styleUrls: ['./frontend.component.css']
})
export class FrontendComponent implements OnInit {

    // public showInviteBand: any = 'true';
    public isUserLoggedIn: boolean = false;
    public loggedInUserData: Array<any> = [];
    public cartItems: Array<any> = [];
    public showDefaultHeader: boolean = false;
    public noShowHeaderPages: Array<string> = ["cart-checkout", "cart-payment", "cart-checkout-summary", "cart-login", "order-confirmation", "reset-password"];
    public noShowFooterPages: Array<string> = [];
    public noShowFooterBottom: boolean = false;
    public currentActivePage: string = this.router.url;
    public productsToRate: Array<any> = [];
    public footerClass: string = (this.router.url == "/") ? "stickyFooter" : "";
    public branchLink: string = BRANCH_LINK;
    public carouselOptions: any = {
        items: 2,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa fa-angle-right' aria-hidden='true'></i>"]
    }

    constructor(
        private userService: UserService,
        private router: Router,
        private ProviderServiceService: ProviderServiceService,
        private angulartics2: Angulartics2
    ) { }

    ngOnInit() {
        // this.userService.setShowInviteBand('true');
        this.updateHeaderCart();
        setInterval(() => {
            if (this.userService.getHeaderCartCheck() == undefined || this.userService.getHeaderCartCheck() == "true") {
                this.updateHeaderCart();
                this.userService.setHeaderCartCheck('false');
            }
        }, 2000);

        setInterval(() => {
            let smartbannerHeight = 0;
            let headerContainerHeight = $(".header-container").height();
            if ($('.smartbanner').css("display") != "none") {
                smartbannerHeight = $('.smartbanner').height();
            }
            $(".custom-margin-div").css({ "margin-top": headerContainerHeight + "px" });
            $('.header-container').css({ top: smartbannerHeight + "px" });
        }, 500);

        setInterval(() => {
            if (this.userService.getLoginPopupCheck() == "true") {
                $("#openLogoutPopup").trigger("click");
                this.userService.setLoginPopupCheck('false');
            }
        }, 500);

        let productToRateCheckTimer = setInterval(() => {
            if (this.userService.getProductToRateCheck() == "true") {
                if (this.isUserLoggedIn) {
                    this.ProviderServiceService.getUserProductsToRate()
                        .subscribe(data => {
                            this.userService.setProductToRateCheck('false');
                            this.productsToRate = data;
                            if (this.productsToRate.length > 0) {
                                $("#openReviewPopup").trigger("click");
                            }
                        }, error => {
                            if (error.statusText == "Unauthorized") {
                                this.userService.setLoginPopupCheck('true');
                            }
                            this.userService.hideAppSpinner();
                        });
                }
            }
        }, 4000);

        setInterval(() => {
            if ($(".qty-nom-hdr").html() == "0") {
                $(".qty-nom-hdr").addClass("hidden");
            } else {
                $(".qty-nom-hdr").removeClass("hidden");
            }
        }, 200);

        this.loggedInUserData = (this.isUserLoggedIn) ? this.userService.getCurrentUserData() : []
        this.isUserLoggedIn = this.userService.isLoggedIn();
        if (this.isUserLoggedIn) {
            this.ProviderServiceService.getUserProductsToRate()
                .subscribe(data => {
                    this.productsToRate = data;
                    if (this.productsToRate.length > 0) {
                        $("#openReviewPopup").trigger("click");
                    }
                }, error => {
                    if (error.statusText == "Unauthorized") {
                        this.userService.setLoginPopupCheck('true');
                    }
                    this.userService.hideAppSpinner();
                });
        }
    }

    ngDoCheck() {
        // this.showInviteBand = this.userService.getShowInviteBand();
        this.currentActivePage = this.router.url;
        this.footerClass = (this.router.url == "/") ? "stickyFooter" : "";
        this.checkShowDefaultHeader();
    }

    public checkShowDefaultHeader() {
        this.showDefaultHeader = $.inArray(this.router.url.split('/')[1], this.noShowHeaderPages) > -1 ? true : false;
        this.noShowFooterBottom = (this.router.url == "/") ? true : false;
    }

    ngAfterContentChecked() {
        this.loggedInUserData = (this.isUserLoggedIn) ? this.userService.getCurrentUserData() : []
        this.isUserLoggedIn = this.userService.isLoggedIn();
    }

    hideInviteBand() {
        // this.userService.setShowInviteBand('false');
    }

    doSignOut() {
        this.ProviderServiceService.logout()
            .subscribe(data => {
                this.userService.unsetAllData();
                this.userService.setHeaderCartCheck('true');
                this.router.navigate(['/']);
            }, error => {
                if (error.statusText == "Unauthorized") {
                    this.userService.setLoginPopupCheck('true');
                }
                this.userService.hideAppSpinner();
            })
    }

    public updateHeaderCart() {
        if (this.userService.isLoggedIn()) {
            this.ProviderServiceService.getShoppingCart()
                .subscribe(data => {
                    this.userService.setShoppingCartData(data);
                    this.userService.setShoppingCartCount(data.length);
                    this.cartItems = data;
                });
        } else {
            let cartLocalStorage = this.userService.getCartLocalStorage();
            this.cartItems = (cartLocalStorage) ? cartLocalStorage : [];
        }
    }

    public onRatingClick(args, productId) {
        this.angulartics2.eventTrack.next({
            action: 'Rate',
            properties: {
                category: 'Rate',
                label: 'action',
                value: "User rated the product",
                productId: productId
            },
        });
        this.ProviderServiceService.submitProductRating(args.rating, productId).subscribe();
    }

    public doForceLogout() {
        this.userService.unsetAllData();
        this.userService.setHeaderCartCheck('true');
        this.router.navigate(['/login']);
    }

}
