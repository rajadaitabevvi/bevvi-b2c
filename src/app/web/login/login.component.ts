import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { AuthService } from "angular4-social-login";
import { FacebookLoginProvider, SocialUser } from "angular4-social-login";
import { UserService } from '../../shared/services/user.service';
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import * as $ from 'jquery';
import * as moment from 'moment';
import { Angulartics2 } from 'angulartics2';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public userCurrentLocation: any = {};
    loginForm: FormGroup;
    signupForm: FormGroup;
    public loggedIn: boolean;
    public currentDate: Date = new Date();
    public currentYear: number = this.currentDate.getFullYear();

    constructor(
        private fb: FormBuilder,
        private ProviderServiceService: ProviderServiceService,
        private router: Router,
        private userService: UserService,
        private authService: AuthService,
        private angulartics2: Angulartics2
    ) {
        this.initForm();
    }

    ngOnInit() {
        this.userService.showAppSpinner();
        if (this.userService.isLoggedIn()) {
            this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                .subscribe(data => {
                    this.userService.setCurrentUserData(data);
                    this.ProviderServiceService.getShoppingCart()
                        .subscribe(data => {
                            this.userService.setShoppingCartData(data);
                            this.ProviderServiceService.getShoppingCartCount()
                                .subscribe(data => {
                                    this.userService.setShoppingCartCount(data.count);
                                    this.ProviderServiceService.getUserOrders()
                                        .subscribe(data => {
                                            this.userService.setUserOrders(data);
                                            this.ProviderServiceService.getUserPickupLocation()
                                                .subscribe(data => {
                                                    this.authService.signOut();
                                                    this.userService.signIn();
                                                    this.userService.setUserPickupLocation(data);
                                                    this.userService.setHeaderCartCheck('true');
                                                    this.userService.setProductToRateCheck('true');
                                                    this.userService.hideAppSpinner();
                                                    this.router.navigate(['/productlisting']);
                                                });
                                        });
                                });
                        });
                }, error => {
                    if (error.statusText == "Unauthorized") {
                        this.userService.setLoginPopupCheck('true');
                    }
                    this.userService.hideAppSpinner();
                });
        } else {
            this.userService.hideAppSpinner();
        }

        this.authService.authState.subscribe((user) => {
            if (user !== null) {
                this.userService.showAppSpinner();
                this.authService.signOut();
                this.ProviderServiceService.checkSocialCredential(user.id)
                    .subscribe(data => {
                        if (data.length == 0) {
                            // if (window.navigator && window.navigator.geolocation) {
                            // window.navigator.geolocation.getCurrentPosition(
                            // position => {
                            let signupData = {
                                firstName: user.firstName,
                                lastName: user.lastName,
                                displayName: user.name,
                                username: user.name,
                                birthday: '',
                                ageRange: (user.age_range_max) ? user.age_range_max : user.age_range_min,
                                location: {
                                    lat: 40.682570,//position.coords.latitude,
                                    lng: -73.930284,//position.coords.longitude
                                },
                                email: user.email,
                                password: 'bevviapp'
                            };

                            this.ProviderServiceService.signup(signupData)
                                .subscribe(data => {
                                    this.ProviderServiceService.createSocialCredential(user.id, data.id)
                                        .subscribe(data => {
                                            this.ProviderServiceService.login({ "email": user.email, "password": 'bevviapp' })
                                                .subscribe(data => {
                                                    this.angulartics2.eventTrack.next({
                                                        action: 'Complete Registration',
                                                        properties: {
                                                            category: 'Complete Registration',
                                                            label: 'action',
                                                            value: "User has Completed Registration",
                                                            accountId: data.userId
                                                        },
                                                    });
                                                    this.angulartics2.eventTrack.next({
                                                        action: 'Login',
                                                        properties: {
                                                            category: 'Login',
                                                            label: 'action',
                                                            value: "User completed Login",
                                                            accountId: data.userId
                                                        },
                                                    });
                                                    this.userService.setAccessToken(data.id);
                                                    this.userService.setCurrentAccountId(data.userId);
                                                    this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                                                        .subscribe(data => {
                                                            this.userService.setCurrentUserData(data);
                                                            this.ProviderServiceService.syncUserCartData(() => {
                                                                this.ProviderServiceService.getShoppingCart()
                                                                    .subscribe(data => {
                                                                        this.userService.setShoppingCartData(data);
                                                                        this.ProviderServiceService.getShoppingCartCount()
                                                                            .subscribe(data => {
                                                                                this.userService.setShoppingCartCount(data.count);
                                                                                this.ProviderServiceService.getUserPickupLocation()
                                                                                    .subscribe(data => {
                                                                                        this.userService.setUserPickupLocation(data);
                                                                                        this.ProviderServiceService.getUserOrders()
                                                                                            .subscribe(data => {
                                                                                                this.userService.setUserOrders(data);
                                                                                                this.ProviderServiceService.getUserSavedCards((data) => {
                                                                                                    this.userService.signIn();
                                                                                                    this.userService.setHeaderCartCheck('true');
                                                                                                    this.userService.setProductToRateCheck('true');
                                                                                                    this.userService.hideAppSpinner();
                                                                                                    this.router.navigate(['/productlisting']);
                                                                                                });
                                                                                            });

                                                                                    });
                                                                            });
                                                                    });

                                                            });
                                                        }, error => {
                                                            this.authService.signOut();
                                                            if (error.statusText == "Unauthorized") {
                                                                this.userService.setLoginPopupCheck('true');
                                                            }
                                                            this.userService.hideAppSpinner();
                                                        });
                                                }, error => {
                                                    window.scrollTo(0, 0);
                                                    this.authService.signOut();
                                                    this.userService.showflashMessage("danger", 'Invalid Credentials. Please check and try again');
                                                    this.userService.unsetAccessToken();
                                                    this.userService.unsetCurrentAccountId();
                                                    this.userService.unsetCurrentUserData();
                                                    this.userService.hideAppSpinner();
                                                });
                                        });
                                },
                                    error => {
                                        window.scrollTo(0, 0);
                                        let errorBody = JSON.parse(error._body);
                                        if (errorBody.error.details.messages.email && errorBody.error.details.messages.email[0] == "Email already exists") {
                                            this.userService.showflashMessage("danger", "Account is already created with Email signup. Please login using your email and password.");
                                        } else if (errorBody.error.details.messages.username) {
                                            this.userService.showflashMessage("danger", errorBody.error.details.messages.username[0]);
                                        } else {
                                            this.userService.showflashMessage("danger", "Some error occured. Please try again");
                                        }
                                        this.userService.unsetAccessToken();
                                        this.userService.unsetCurrentAccountId();
                                        this.userService.unsetCurrentUserData();
                                        this.userService.hideAppSpinner();
                                    });
                        } else {
                            this.ProviderServiceService.login({ "email": user.email, "password": 'bevviapp' })
                                .subscribe(data => {
                                    this.userService.setAccessToken(data.id);
                                    this.userService.setCurrentAccountId(data.userId);
                                    this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                                        .subscribe(data => {
                                            this.userService.setCurrentUserData(data);
                                            this.ProviderServiceService.syncUserCartData(() => {
                                                this.ProviderServiceService.getShoppingCart()
                                                    .subscribe(data => {
                                                        this.angulartics2.eventTrack.next({
                                                            action: 'Login',
                                                            properties: {
                                                                category: 'Login',
                                                                label: 'action',
                                                                value: "User completed Login",
                                                                accountId: this.userService.getCurrentAccountId()
                                                            },
                                                        });
                                                        this.userService.setShoppingCartData(data);
                                                        this.ProviderServiceService.getShoppingCartCount()
                                                            .subscribe(data => {
                                                                this.userService.setShoppingCartCount(data.count);
                                                                this.ProviderServiceService.getUserOrders()
                                                                    .subscribe(data => {
                                                                        this.userService.setUserOrders(data);
                                                                        this.ProviderServiceService.getUserPickupLocation()
                                                                            .subscribe(data => {
                                                                                this.userService.signIn();
                                                                                this.authService.signOut();
                                                                                this.userService.setUserPickupLocation(data);
                                                                                this.userService.setHeaderCartCheck('true');
                                                                                this.userService.setProductToRateCheck('true');
                                                                                this.userService.hideAppSpinner();
                                                                                this.router.navigate(['/productlisting']);
                                                                            });
                                                                    });
                                                            });
                                                    });
                                            });
                                        }, error => {
                                            if (error.statusText == "Unauthorized") {
                                                this.userService.setLoginPopupCheck('true');
                                            }
                                            this.userService.hideAppSpinner();
                                        });
                                }, error => {
                                    window.scrollTo(0, 0);
                                    this.userService.showflashMessage("danger", 'Invalid Credentials. Please check and try again');
                                    this.userService.unsetAccessToken();
                                    this.userService.unsetCurrentAccountId();
                                    this.userService.unsetCurrentUserData();
                                    this.userService.hideAppSpinner();
                                });
                        }
                    });
            }
        });
    }

    initForm() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email, Validators.minLength(5)]],
            password: ['', [Validators.required]]
        });

        this.signupForm = this.fb.group({
            username: ['', [Validators.required]],
            s_email: ['', [Validators.required, Validators.email]],
            s_password: ['', [Validators.required]],
            confirm_password: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
            date: ['', [Validators.required, Validators.min(1), Validators.max(31)]],
            month: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
            year: ['', [Validators.required, Validators.min(1800), Validators.max(this.currentYear - 18)]]
        }, { validator: this.userService.checkIfMatchingPasswords('s_password', 'confirm_password') })
    }

    get email() { return this.loginForm.get('email'); }

    get password() { return this.loginForm.get('password'); }

    get s_email() { return this.signupForm.get('s_email'); }

    get username() { return this.signupForm.get('username'); }

    get s_password() { return this.signupForm.get('s_password'); }

    get confirm_password() { return this.signupForm.get('confirm_password'); }

    get phoneNumber() { return this.signupForm.get('phoneNumber'); }

    get date() { return this.signupForm.get('date'); }

    get month() { return this.signupForm.get('month'); }

    get year() { return this.signupForm.get('year'); }

    onLogin() {
        this.userService.showAppSpinner();
        // this.ProviderServiceService.login({ "email": this.loginForm.get('email').value, "password": this.loginForm.get('password').value })
        this.ProviderServiceService.emailLogin(this.loginForm.get('email').value, this.loginForm.get('password').value)
            .subscribe(data => {
                this.userService.setAccessToken(data[0].id);
                this.userService.setCurrentAccountId(data[0].userId);
                this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
                    .subscribe(data => {
                        this.userService.setCurrentUserData(data);
                        this.ProviderServiceService.syncUserCartData(() => {
                            this.ProviderServiceService.getShoppingCart()
                                .subscribe(data => {
                                    this.loginForm.reset();
                                    this.angulartics2.eventTrack.next({
                                        action: 'Login',
                                        properties: {
                                            category: 'Login',
                                            label: 'action',
                                            value: "User completed Login",
                                            accountId: this.userService.getCurrentAccountId()
                                        },
                                    });
                                    this.userService.setShoppingCartData(data);
                                    this.ProviderServiceService.getShoppingCartCount()
                                        .subscribe(data => {
                                            this.userService.setShoppingCartCount(data.count);
                                            this.ProviderServiceService.getUserOrders()
                                                .subscribe(data => {
                                                    this.userService.setUserOrders(data);
                                                    this.ProviderServiceService.getUserPickupLocation()
                                                        .subscribe(data => {
                                                            this.userService.signIn();
                                                            this.userService.setUserPickupLocation(data);
                                                            this.userService.setHeaderCartCheck('true');
                                                            this.userService.setProductToRateCheck('true');
                                                            this.userService.hideAppSpinner();
                                                            this.router.navigate(['/productlisting']);
                                                        });
                                                });
                                        });
                                });
                        })
                    }, error => {
                        if (error.statusText == "Unauthorized") {
                            this.userService.setLoginPopupCheck('true');
                        }
                        this.userService.hideAppSpinner();
                    });
            }, error => {
                window.scrollTo(0, 0);
                this.userService.showflashMessage("danger", JSON.parse(error._body).error.message);
                this.userService.unsetAccessToken();
                this.userService.unsetCurrentAccountId();
                this.userService.unsetCurrentUserData();
                this.userService.hideAppSpinner();
            });
    }

    onSignup(): void {
        this.userService.showAppSpinner();
        if (window.navigator && window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(
                position => {
                    let firstName: string = this.signupForm.get('username').value.split(' ')[0];
                    let lastName: string = (this.signupForm.get('username').value.split(' ')[1]) ? this.signupForm.get('username').value.split(' ')[1] : '';

                    let signupData = {
                        firstName: firstName,
                        lastName: lastName,
                        phoneNumber: this.signupForm.get('phoneNumber').value,
                        displayName: firstName + '.' + lastName,
                        username: this.signupForm.get('username').value,
                        birthday: moment(this.signupForm.controls.month.value + "-" + this.signupForm.controls.date.value + "-" + this.signupForm.controls.year.value).format('YYYY-MM-DD'),
                        ageRange: moment().diff(moment(this.signupForm.controls.month.value + "-" + this.signupForm.controls.date.value + "-" + this.signupForm.controls.year.value).format('YYYY-MM-DD'), 'years'),
                        location: {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        },
                        email: this.signupForm.get('s_email').value,
                        password: this.signupForm.get('s_password').value
                    };
                    this.ProviderServiceService.signup(signupData)
                        .subscribe(data => {
                            window.scrollTo(0, 0);
                            this.signupForm.reset();
                            this.userService.showflashMessage('success', 'Signup Successfull. Please login with with your credentials');
                            this.userService.hideAppSpinner();
                        },
                            error => {
                                window.scrollTo(0, 0);
                                let errorBody = JSON.parse(error._body);
                                if (errorBody.error.details.messages.email && errorBody.error.details.messages.email[0] == "Email already exists") {
                                    this.userService.showflashMessage("danger", "Account is already created with Email signup. Please login using your email and password.");
                                } else if (errorBody.error.details.messages.username) {
                                    this.userService.showflashMessage("danger", errorBody.error.details.messages.username[0]);
                                } else {
                                    this.userService.showflashMessage("danger", "Some error occured. Please try again");
                                }
                                this.userService.unsetAccessToken();
                                this.userService.unsetCurrentAccountId();
                                this.userService.unsetCurrentUserData();
                                this.userService.hideAppSpinner();
                            });
                },
                error => {
                    let signupData = {
                        firstName: this.signupForm.get('username').value.split(' ')[0],
                        lastName: (this.signupForm.get('username').value.split(' ')[1]) ? this.signupForm.get('username').value.split(' ')[1] : '',
                        phoneNumber: this.signupForm.get('phoneNumber').value,
                        displayName: this.signupForm.get('username').value,
                        username: this.signupForm.get('username').value,
                        birthday: moment(this.signupForm.controls.month.value + "-" + this.signupForm.controls.date.value + "-" + this.signupForm.controls.year.value).format('YYYY-MM-DD'),
                        ageRange: moment().diff(moment(this.signupForm.controls.month.value + "-" + this.signupForm.controls.date.value + "-" + this.signupForm.controls.year.value).format('YYYY-MM-DD'), 'years'),
                        location: {
                            lat: 40.682570,
                            lng: -73.930284
                        },
                        email: this.signupForm.get('s_email').value,
                        password: this.signupForm.get('s_password').value
                    };
                    this.ProviderServiceService.signup(signupData)
                        .subscribe(data => {
                            window.scrollTo(0, 0);
                            this.signupForm.reset();
                            this.userService.showflashMessage('success', 'Signup Successfull. Please login with with your credentials');
                            this.userService.hideAppSpinner();
                        },
                            error => {
                                window.scrollTo(0, 0);
                                let errorBody = JSON.parse(error._body);
                                if (errorBody.error.details.messages.email && errorBody.error.details.messages.email[0] == "Email already exists") {
                                    this.userService.showflashMessage("danger", "Account is already created with Email signup. Please login using your email and password.");
                                } else if (errorBody.error.details.messages.username) {
                                    this.userService.showflashMessage("danger", errorBody.error.details.messages.username[0]);
                                } else {
                                    this.userService.showflashMessage("danger", "Some error occured. Please try again");
                                }
                                this.userService.unsetAccessToken();
                                this.userService.unsetCurrentAccountId();
                                this.userService.unsetCurrentUserData();
                                this.userService.hideAppSpinner();
                            });
                }
            );
        };
        // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }

    signInWithFB(): void {
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }

}
