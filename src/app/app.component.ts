import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    constructor(
        angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
        private router: Router
    ) { }

    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    }

    ngAfterViewInit() {
        new SmartBanner({
            daysHidden: 0,   // days to hide banner after close button is clicked (defaults to 15)
            daysReminder: 0, // days to hide banner after "VIEW" button is clicked (defaults to 90)
            appStoreLanguage: 'us', // language code for the App Store (defaults to user's browser language)
            title: 'Bevvi',
            author: 'Etail Inc',
            button: 'VIEW',
            store: {
                ios: 'On the App Store',
                android: 'In Google Play',
            },
            price: {
                ios: 'FREE',
                android: 'FREE',
            },
            //theme: 'android' // put platform type ('ios', 'android', etc.) here to force single theme on all device
        });
    }

    title = 'Bevvi';
}
