import { UserService } from './../services/user.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'calculateTotalPrice'
})
export class CalculateTotalPricePipe implements PipeTransform {
    constructor(
        private userService: UserService
    ) { }

    public totalPrice: number = 0;
    public taxRate: number = 0;
    public discountInfo: number = 0;

    transform(cartItems: Array<any>): any {
        this.taxRate = this.userService.getTaxRate();
        this.discountInfo = this.userService.getDiscountInfo();
        this.totalPrice = 0;

        cartItems.forEach((items) => {
            this.totalPrice += (items.offer.salePrice) * items.quantity;
        });

        if (!isNaN(this.taxRate)) {
            this.totalPrice = this.totalPrice + (this.totalPrice * this.taxRate);
        }

        if (this.discountInfo) {
            this.totalPrice -= this.discountInfo;
        }

        return (this.totalPrice < 0) ? 0 : this.totalPrice.toFixed(2);
    }

}
