import { UserService } from './../services/user.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'calculateTax'
})
export class CalculateTaxPipe implements PipeTransform {
    constructor(
        private userService: UserService
    ) { }

    public totalPrice: number = 0;
    public taxRate: number = 0;
    public discountInfo: number = 0;

    transform(cartItems: Array<any>): any {
        this.totalPrice = 0;
        this.taxRate = this.userService.getTaxRate();
        this.discountInfo = this.userService.getDiscountInfo();

        cartItems.forEach((items) => {
            this.totalPrice += (items.offer.salePrice) * items.quantity;
        });

        // if (this.discountInfo) {
        //     this.totalPrice -= this.discountInfo;
        // }

        this.totalPrice = (this.totalPrice * this.taxRate);

        this.userService.setTaxPrice((this.totalPrice < 0) ? 0 : this.totalPrice.toFixed(2));
        return (this.totalPrice < 0) ? 0 : this.totalPrice.toFixed(2);
    }

}
