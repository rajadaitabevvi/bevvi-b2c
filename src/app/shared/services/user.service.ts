import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SECRETKEY } from '../../constants/constants';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import * as encrypt from "encryptjs";
import { FlashMessagesService } from 'angular2-flash-messages';
import { FormGroup, ValidationErrors } from '@angular/forms';
import { SpinnerService } from '@chevtek/angular-spinners';
import * as moment from 'moment';

@Injectable()
export class UserService {

    constructor(
        private http: HttpClient,
        private _flashMessagesService: FlashMessagesService,
        private spinnerService: SpinnerService
    ) { }

    checkUserAge(age) {
        return (fg: FormGroup): ValidationErrors => {
            let result: ValidationErrors = null;
            if (fg.get('year').valid && fg.get('month').valid && fg.get('day').valid) {
                const value: { year: string, month: string, day: string } = fg.value;
                const date = moment({ year: +value.year, month: (+value.month) - 1, day: +value.day }).startOf('day');
                if (date.isValid()) {
                    const now = moment().startOf('day');
                    const yearsDiff = date.diff(now, 'years');
                    if (yearsDiff > -age) {
                        result = {
                            'minimumAge': {
                                'requiredAge': age,
                                'actualAge': yearsDiff
                            }
                        };
                    }
                }
            }
            return result;
        };
    }

    checkIfMatchingPasswords(password: string, confirm_password: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[password];
            let passwordConfirmationInput = group.controls[confirm_password];
            const errors = passwordConfirmationInput.errors;

            if (passwordInput.value !== passwordConfirmationInput.value) {
                let newErrors = { notEquivalent: true };
                if (errors) {
                    newErrors = Object.assign(errors, newErrors);
                }
                return passwordConfirmationInput.setErrors(newErrors);
            } else {
                if (passwordConfirmationInput.errors) {
                    delete passwordConfirmationInput.errors['notEquivalent'];
                }
                if (passwordConfirmationInput.errors && Object.keys(passwordConfirmationInput.errors).length == 0) {
                    passwordConfirmationInput.setErrors(null);
                }
                return;
            }
        }
    }

    setAccessToken(data): void { localStorage.setItem('accessToken', this.encrypt(data)); }

    unsetAccessToken(): void { localStorage.removeItem('accessToken'); }

    getAccessToken(): string { return this.decrypt(localStorage.getItem("accessToken")); }

    setCurrentEstablishmentId(data): void { localStorage.setItem('currentEstablishmentId', this.encrypt(data)); }

    unsetCurrentEstablishmentId(): void { localStorage.removeItem('currentEstablishmentId'); }

    getCurrentEstablishmentId(): string { return this.decrypt(localStorage.getItem("currentEstablishmentId")); }

    setCartEstablishmentId(data): void { localStorage.setItem('cartEstablishmentId', this.encrypt(data)); }

    unsetCartEstablishmentId(): void { localStorage.removeItem('cartEstablishmentId'); }

    getCartEstablishmentId(): string { return this.decrypt(localStorage.getItem("cartEstablishmentId")); }

    setShoppingCartData(data): void {
        localStorage.setItem('shoppingCartData', this.encrypt(JSON.stringify(data)));
        if (data.length > 0) {
            this.setCartEstablishmentId(data[0].offer.establishmentId);
        }
    }

    unsetShoppingCartData(): void { localStorage.removeItem('shoppingCartData'); }

    getShoppingCartData(): string { return this.decrypt(localStorage.getItem("shoppingCartData")); }

    setShoppingCartCount(data): void {
        localStorage.setItem('shoppingCartCount', data);
        if (data == 0) {
            this.unsetCartEstablishmentId();
        }
    }

    getUserPickupLocation(): Array<any> { return JSON.parse(this.decrypt(localStorage.getItem("userPickupLocation"))); }

    setUserPickupLocation(data): void { localStorage.setItem('userPickupLocation', this.encrypt(JSON.stringify(data))); }

    unsetUserPickupLocation(): void { localStorage.removeItem('userPickupLocation'); }



    getUserPickupLocationToUse(): any { return JSON.parse(this.decrypt(localStorage.getItem("userPickupLocationToUse"))); }

    setUserPickupLocationToUse(data): void { localStorage.setItem('userPickupLocationToUse', this.encrypt(JSON.stringify(data))); }

    unsetUserPickupLocationToUse(): void { localStorage.removeItem('userPickupLocationToUse'); }



    getUserOrders(): string { return this.decrypt(localStorage.getItem("userOrders")); }

    setUserOrders(data): void { localStorage.setItem('userOrders', this.encrypt(JSON.stringify(data))); }

    unsetUserOrders(): void { localStorage.removeItem('userOrders'); }

    unsetShoppingCartCount(): void { localStorage.removeItem('shoppingCartCount'); }

    getShoppingCartCount(): string { return localStorage.getItem("shoppingCartCount"); }

    setCurrentAccountId(data): void { localStorage.setItem('currentAccountId', this.encrypt(data)); }

    getCurrentAccountId(): string { return this.decrypt(localStorage.getItem("currentAccountId")); }

    unsetCurrentAccountId(): void { localStorage.removeItem('currentAccountId'); }

    setCurrentUserData(data): void {
        localStorage.setItem('currentUser', this.encrypt(JSON.stringify(data)));
        if (data.paymentIds && data.paymentIds.length > 0) {
            this.setCurrentUserPayment(data.paymentIds[0].token);
        } else {
            this.unsetCurrentUserPayment();
        }

        if (data.userProfiles) {
            this.setUserRadiusMiles(data.userProfiles.radius);
            this.setUserTransportmode(data.userProfiles.transport);
        } else {
            this.setUserRadiusMiles(5);
            this.setUserTransportmode(0);
        }
    }

    unsetCurrentUserData(): void { localStorage.removeItem('currentUser'); }

    getCurrentUserData(): Array<any> { return JSON.parse(this.decrypt(localStorage.getItem("currentUser"))); }



    setCurrentUserPayment(data): void {
        localStorage.setItem('currentUserPayment', this.encrypt(JSON.stringify(data)));
    }

    unsetCurrentUserPayment(): void { localStorage.removeItem('currentUserPayment'); }

    getCurrentUserPayment(): Array<any> { return JSON.parse(this.decrypt(localStorage.getItem("currentUserPayment"))); }

    setUserRadiusMiles(data): void {
        localStorage.setItem('userRadiusMiles', this.encrypt(JSON.stringify(data)));
    }

    unsetUserRadiusMiles(): void { localStorage.removeItem('userRadiusMiles'); }

    getUserRadiusMiles(): number { return parseFloat(this.decrypt(localStorage.getItem("userRadiusMiles"))); }

    setUserTransportmode(data): void {
        localStorage.setItem('userTransportmode', this.encrypt(JSON.stringify(data)));
    }

    unsetUserTransportmode(): void { localStorage.removeItem('userTransportmode'); }

    getUserTransportmode(): number { return parseInt(this.decrypt(localStorage.getItem("userTransportmode"))); }



    setUserPickupDateTime(data): void {
        localStorage.setItem('userPickupDateTime', data);
    }

    unsetUserPickupDateTime(): void { localStorage.removeItem('userPickupDateTime'); }

    getUserPickupDateTime(): string { return localStorage.getItem("userPickupDateTime"); }



    setUserPaymentCard(data): void {
        localStorage.setItem('userPaymentCard', data);
    }

    unsetUserPaymentCard(): void { localStorage.removeItem('userPaymentCard'); }

    getUserPaymentCard(): string { return localStorage.getItem("userPaymentCard"); }


    setTaxRate(data): void {
        localStorage.setItem('taxRate', data);
    }

    unsetTaxRate(): void { localStorage.removeItem('taxRate'); }

    getTaxRate(): number { return parseFloat(localStorage.getItem("taxRate")); }

    setTaxPrice(data): void {
        localStorage.setItem('taxPrice', data);
    }

    unsetTaxPrice(): void { localStorage.removeItem('taxPrice'); }

    getTaxPrice(): number { return parseFloat(localStorage.getItem("taxPrice")); }


    setDiscountInfo(data): void {
        localStorage.setItem('discountInfo', data);
    }

    setDiscountJson(data): void {
        localStorage.setItem('discountJson', this.encrypt(JSON.stringify(data)));
    }

    setDefaultDiscountJson(): void {
        localStorage.setItem('discountJson', this.encrypt(JSON.stringify(
            {
                "discountAmount": 0,
                "discountUsed": "No Discounts",
                "disctype": 0
            }
        )))
    }

    unsetDiscountInfo(): void {
        localStorage.removeItem('discountInfo');
        localStorage.removeItem('discountJson');
    }

    unsetDiscountJson(): void {
        localStorage.removeItem('discountJson');
    }

    getDiscountInfo(): number { return parseFloat(localStorage.getItem("discountInfo")); }

    getDiscountJson(): void { return JSON.parse(this.decrypt(localStorage.getItem("discountJson"))); }



    showflashMessage(type: string, message: string, time: number = 5000) {
        this._flashMessagesService.show(message, { cssClass: 'alert-' + type, timeout: time });
    }

    showAppSpinner() {
        $('spinner').addClass("spinnerActive");
        this.spinnerService.show('global_spinner');
    }

    hideAppSpinner() {
        $('spinner').removeClass("spinnerActive");
        this.spinnerService.hide('global_spinner');
    }

    setHeaderCartCheck(data: string) {
        localStorage.setItem('headerCartCheck', data);
    }

    getHeaderCartCheck() {
        return localStorage.getItem('headerCartCheck');
    }

    setLoginPopupCheck(data: string) {
        localStorage.setItem('loginPopupCheck', data);
    }

    getLoginPopupCheck() {
        return localStorage.getItem('loginPopupCheck');
    }

    setShowInviteBand(data: string) {
        localStorage.setItem('showInviteBand', data);
    }

    getShowInviteBand() {
        return localStorage.getItem('showInviteBand');
    }

    setProductToRateCheck(data: string) {
        localStorage.setItem('productToRateCheck', data);
    }

    getProductToRateCheck() {
        return localStorage.getItem('productToRateCheck');
    }

    encrypt(data): string {
        return data;
        // return encrypt.encrypt(data, SECRETKEY, 256);
    }

    decrypt(data): string {
        return data;
        // return encrypt.decrypt(data, SECRETKEY, 256);
    }

    isLoggedIn(): boolean {
        return (localStorage.getItem('currentAccountId') && localStorage.getItem('accessToken')) ? true : false;
    }

    signOut(): boolean {
        this.unsetAllData();
        return true;
    }

    signIn(): boolean {
        this.setShowInviteBand('true');
        return true;
    }

    getCartLocalStorage() {
        let localData = localStorage.getItem("cartLocalStorage");
        if (localData == null) {
            return [];
        } else {
            return JSON.parse(localStorage.getItem("cartLocalStorage"));
        }
    }

    setCartLocalStorage(cartData: any) {
        let cartStorage = JSON.parse(localStorage.getItem("cartLocalStorage"));
        if (cartStorage == null) {
            cartStorage = [cartData];
        } else {
            cartStorage.push(cartData);
        }
        localStorage.setItem("cartLocalStorage", JSON.stringify(cartStorage));
    }

    replaceCartLocalStorage(cartData: any) {
        localStorage.setItem("cartLocalStorage", JSON.stringify(cartData));
    }

    unsetCartLocalStorage() {
        localStorage.removeItem("cartLocalStorage");
    }

    indexOfAny = function (s, arr, begin) {
        var minIndex = -1;
        for (var i = 0; i < arr.length; i++) {
            var index = s.indexOf(arr[i], begin);
            if (index != -1) {
                if (minIndex == -1 || index < minIndex) {
                    minIndex = index;
                }
            }
        }
        return (minIndex);
    }

    splitByAny = function (s, arr) {
        var parts = [];

        var index;
        do {
            index = this.indexOfAny(s, arr);
            if (index != -1) {
                parts.push(s.substr(0, index));
                s = s.substr(index + 1);
            } else {
                parts.push(s);
            }
        } while (index != -1);

        return (parts);
    }

    parseAddress(address) {
        var obj = {
            address: "",
            city: "",
            state: "",
            postalCode: "",
            country: ""
        };

        if (!address) {
            return (obj);
        }

        var parts = address.split(',');
        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i].trim();
        }
        var i = parts.length - 1;

        var fnIsPostalCode = function (value) {
            return (/^\d+$/.test(value));
        }

        var fnParsePostalCode = (value) => {
            var subParts = this.splitByAny(value, [' ', '-']);
            for (var j = 0; j < subParts.length; j++) {
                if (fnIsPostalCode(subParts[j].trim())) {
                    obj.postalCode = subParts[j].trim();
                    if (j > 0) {
                        return (subParts[j - 1]);
                        // break;
                    }
                }
            }
            return (value);
        }

        if (i >= 0) {
            if (fnIsPostalCode(parts[i])) { obj.postalCode = parts[i]; i--; }
            var part = fnParsePostalCode(parts[i]);
            if (part) { obj.country = part; }
            i--;
        }

        if (i >= 0) {
            if (fnIsPostalCode(parts[i])) { obj.postalCode = parts[i]; i--; }
            var part = fnParsePostalCode(parts[i]);
            if (part) { obj.state = part; }
            i--;
        }

        if (i >= 0) {
            if (fnIsPostalCode(parts[i])) { obj.postalCode = parts[i]; i--; }
            var part = fnParsePostalCode(parts[i]);
            if (part) { obj.city = part; }
            i--;
        }

        if (i >= 0) {
            parts = parts.slice(0, i + 1);
            obj.address = parts.join(', ');
        }

        return (obj);
    }

    unsetAllData() {
        this.unsetAccessToken();
        this.unsetCurrentAccountId();
        this.unsetCurrentEstablishmentId();
        this.unsetCurrentUserData();
        this.unsetShoppingCartCount();
        this.unsetShoppingCartData();
        this.unsetCartEstablishmentId();
        this.unsetUserOrders();
        this.unsetUserPickupLocation();
        this.unsetCurrentUserPayment();
        this.unsetDiscountInfo();
        this.unsetUserPickupLocationToUse();
        this.unsetCurrentUserPayment();
        this.setShowInviteBand('true');

        this.afterCheckout();
    }

    afterCheckout() {
        this.unsetCartEstablishmentId();
        this.unsetShoppingCartCount();
        this.unsetShoppingCartData();
        this.unsetUserPaymentCard();
        this.unsetUserPickupDateTime();
        this.unsetDiscountInfo();
    }
}